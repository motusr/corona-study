---
title: "Experimental mobility study - DE - end survey"
author: "Ilse Laurijssen"
date: "25 April, 2022 19:00"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "25 April, 2022 19:00"
* cleaned data: "25 April, 2022 19:00"

## Response

* Total number of respondents: 9

* Respondents who started the diary: 


* Respondents who finished the research: 9

### Finished data states over time (cumulative)


```
## No data available for finished data states
```

### Activity last week


### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/309-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|           | finished| busy|
|:----------|--------:|----:|
|Invitation |        9|    0|
|End Survey |        0|    9|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```


















