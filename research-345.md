---
title: "Femma vervolgonderzoek 2023 (maart"
author: "Ilse Laurijssen"
date: "28 December, 2023 19:00"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "28 December, 2023 19:00"
* cleaned data: "28 December, 2023 19:00"

## Response

* Total number of respondents: 54

* Respondents who started the diary: 


* Respondents who finished the research: 47

### Finished data states over time (cumulative)

![plot of chunk progress](figures/345-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/345-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                      | finished| busy|
|:---------------------|--------:|----:|
|Inlezen               |       54|    0|
|Uitnodiging           |       49|    5|
|Vragenlijst           |       47|    2|
|Bedankt voor deelname |       47|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```


















