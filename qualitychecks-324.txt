      resp          dstchange   starthour starthourscore registeredtime 
 Min.   :169744   Min.   :0   Min.   :4   Min.   :1      Min.   : 10.0  
 1st Qu.:170220   1st Qu.:0   1st Qu.:4   1st Qu.:1      1st Qu.:227.5  
 Median :170696   Median :0   Median :4   Median :1      Median :445.0  
 Mean   :170696   Mean   :0   Mean   :4   Mean   :1      Mean   :445.0  
 3rd Qu.:171172   3rd Qu.:0   3rd Qu.:4   3rd Qu.:1      3rd Qu.:662.5  
 Max.   :171648   Max.   :0   Max.   :4   Max.   :1      Max.   :880.0  
                                                                        
 registeredtimescore  missingtime   missingtimescore   numberofregistrations
 Min.   :0.003472    Min.   :2000   Min.   :0.003472   Min.   : 1.00        
 1st Qu.:0.078993    1st Qu.:2218   1st Qu.:0.078993   1st Qu.: 3.25        
 Median :0.154514    Median :2435   Median :0.154514   Median : 5.50        
 Mean   :0.154514    Mean   :2435   Mean   :0.154514   Mean   : 5.50        
 3rd Qu.:0.230035    3rd Qu.:2652   3rd Qu.:0.230035   3rd Qu.: 7.75        
 Max.   :0.305556    Max.   :2870   Max.   :0.305556   Max.   :10.00        
                                                                            
 numberofsleep  numberofeat  qualitychecks     qualityscore     numberofdays
 Min.   : NA   Min.   : NA   Min.   :0.3333   Min.   :0.3356   Min.   :1    
 1st Qu.: NA   1st Qu.: NA   1st Qu.:0.3333   1st Qu.:0.3860   1st Qu.:1    
 Median : NA   Median : NA   Median :0.3333   Median :0.4363   Median :1    
 Mean   :NaN   Mean   :NaN   Mean   :0.3333   Mean   :0.4363   Mean   :1    
 3rd Qu.: NA   3rd Qu.: NA   3rd Qu.:0.3333   3rd Qu.:0.4867   3rd Qu.:1    
 Max.   : NA   Max.   : NA   Max.   :0.3333   Max.   :0.5370   Max.   :1    
 NA's   :2     NA's   :2                                                    
      resp        dstchange.day1 starthour.day1 starthourscore.day1
 Min.   :169744   Min.   :0      Min.   :4      Min.   :1          
 1st Qu.:170220   1st Qu.:0      1st Qu.:4      1st Qu.:1          
 Median :170696   Median :0      Median :4      Median :1          
 Mean   :170696   Mean   :0      Mean   :4      Mean   :1          
 3rd Qu.:171172   3rd Qu.:0      3rd Qu.:4      3rd Qu.:1          
 Max.   :171648   Max.   :0      Max.   :4      Max.   :1          
                                                                   
 registeredtime.day1 registeredtimescore.day1 missingtime.day1
 Min.   : 10.0       Min.   :0.006944         Min.   : 560.0  
 1st Qu.:227.5       1st Qu.:0.157986         1st Qu.: 777.5  
 Median :445.0       Median :0.309028         Median : 995.0  
 Mean   :445.0       Mean   :0.309028         Mean   : 995.0  
 3rd Qu.:662.5       3rd Qu.:0.460069         3rd Qu.:1212.5  
 Max.   :880.0       Max.   :0.611111         Max.   :1430.0  
                                                              
 missingtimescore.day1 numberofregistrations.day1 numberofsleep.day1
 Min.   :0.006944      Min.   : 1.00              Min.   : NA       
 1st Qu.:0.157986      1st Qu.: 3.25              1st Qu.: NA       
 Median :0.309028      Median : 5.50              Median : NA       
 Mean   :0.309028      Mean   : 5.50              Mean   :NaN       
 3rd Qu.:0.460069      3rd Qu.: 7.75              3rd Qu.: NA       
 Max.   :0.611111      Max.   :10.00              Max.   : NA       
                                                  NA's   :2         
 numberofeat.day1 qualitychecks.day1 qualityscore.day1
 Min.   : NA      Min.   :0.3333     Min.   :0.3380   
 1st Qu.: NA      1st Qu.:0.3333     1st Qu.:0.4387   
 Median : NA      Median :0.3333     Median :0.5394   
 Mean   :NaN      Mean   :0.3333     Mean   :0.5394   
 3rd Qu.: NA      3rd Qu.:0.3333     3rd Qu.:0.6400   
 Max.   : NA      Max.   :0.3333     Max.   :0.7407   
 NA's   :2                                            
