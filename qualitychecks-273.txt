      resp          dstchange   starthour      starthourscore   registeredtime
 Min.   :165179   Min.   :0   Min.   : 4.000   Min.   :0.0000   Min.   : 240  
 1st Qu.:168071   1st Qu.:0   1st Qu.: 4.000   1st Qu.:1.0000   1st Qu.:1275  
 Median :168079   Median :0   Median : 4.000   Median :1.0000   Median :2880  
 Mean   :167821   Mean   :0   Mean   : 4.804   Mean   :0.8696   Mean   :2399  
 3rd Qu.:168140   3rd Qu.:0   3rd Qu.: 4.000   3rd Qu.:1.0000   3rd Qu.:3090  
 Max.   :168202   Max.   :0   Max.   :13.000   Max.   :1.0000   Max.   :4504  
                                                                              
 registeredtimescore  missingtime      missingtimescore  numberofregistrations
 Min.   :0.08333     Min.   :-1440.0   Min.   :0.08333   Min.   :  3.00       
 1st Qu.:0.44271     1st Qu.: -210.5   1st Qu.:0.44271   1st Qu.:  9.50       
 Median :1.00000     Median :    0.0   Median :1.00000   Median : 11.00       
 Mean   :0.74947     Mean   :  533.3   Mean   :0.74046   Mean   : 20.04       
 3rd Qu.:1.00000     3rd Qu.: 1605.0   3rd Qu.:1.00000   3rd Qu.: 20.00       
 Max.   :1.00000     Max.   : 2640.0   Max.   :1.00000   Max.   :107.00       
                                                                              
 numberofsleep  numberofeat  qualitychecks     qualityscore      numberofdays  
 Min.   : NA   Min.   : NA   Min.   :0.0000   Min.   :0.05556   Min.   :1.000  
 1st Qu.: NA   1st Qu.: NA   1st Qu.:0.3333   1st Qu.:0.60301   1st Qu.:1.000  
 Median : NA   Median : NA   Median :1.0000   Median :1.00000   Median :2.000  
 Mean   :NaN   Mean   :NaN   Mean   :0.6957   Mean   :0.78650   Mean   :1.652  
 3rd Qu.: NA   3rd Qu.: NA   3rd Qu.:1.0000   3rd Qu.:1.00000   3rd Qu.:2.000  
 Max.   : NA   Max.   : NA   Max.   :1.0000   Max.   :1.00000   Max.   :3.000  
 NA's   :23    NA's   :23                                                      
      resp        dstchange.day1 starthour.day1   starthourscore.day1
 Min.   :165179   Min.   :0      Min.   : 4.000   Min.   :0.0000     
 1st Qu.:168071   1st Qu.:0      1st Qu.: 4.000   1st Qu.:1.0000     
 Median :168079   Median :0      Median : 4.000   Median :1.0000     
 Mean   :167821   Mean   :0      Mean   : 4.804   Mean   :0.8696     
 3rd Qu.:168140   3rd Qu.:0      3rd Qu.: 4.000   3rd Qu.:1.0000     
 Max.   :168202   Max.   :0      Max.   :13.000   Max.   :1.0000     
                                                                     
 registeredtime.day1 registeredtimescore.day1 missingtime.day1 
 Min.   : 240        Min.   :0.1667           Min.   :-1440.0  
 1st Qu.:1170        1st Qu.:0.8125           1st Qu.:-1440.0  
 Median :2874        Median :1.0000           Median :-1434.0  
 Mean   :2088        Mean   :0.8582           Mean   : -647.9  
 3rd Qu.:2880        3rd Qu.:1.0000           3rd Qu.:  270.0  
 Max.   :2880        Max.   :1.0000           Max.   : 1200.0  
                                                               
 missingtimescore.day1 numberofregistrations.day1 numberofsleep.day1
 Min.   :0.1667        Min.   :  2.00             Min.   : NA       
 1st Qu.:0.8125        1st Qu.:  8.00             1st Qu.: NA       
 Median :1.0000        Median : 10.00             Median : NA       
 Mean   :0.8582        Mean   : 17.13             Mean   :NaN       
 3rd Qu.:1.0000        3rd Qu.: 19.00             3rd Qu.: NA       
 Max.   :1.0000        Max.   :107.00             Max.   : NA       
                                                  NA's   :23        
 numberofeat.day1 qualitychecks.day1 qualityscore.day1 dstchange.day2
 Min.   : NA      Min.   :0.0000     Min.   :0.1111    Min.   :0     
 1st Qu.: NA      1st Qu.:0.3333     1st Qu.:0.8727    1st Qu.:0     
 Median : NA      Median :1.0000     Median :1.0000    Median :0     
 Mean   :NaN      Mean   :0.7246     Mean   :0.8620    Mean   :0     
 3rd Qu.: NA      3rd Qu.:1.0000     3rd Qu.:1.0000    3rd Qu.:0     
 Max.   : NA      Max.   :1.0000     Max.   :1.0000    Max.   :0     
 NA's   :23                                            NA's   :11    
 starthour.day2 starthourscore.day2 registeredtime.day2
 Min.   :4      Min.   :1           Min.   : 146.0     
 1st Qu.:4      1st Qu.:1           1st Qu.: 198.8     
 Median :4      Median :1           Median : 230.5     
 Mean   :4      Mean   :1           Mean   : 494.8     
 3rd Qu.:4      3rd Qu.:1           3rd Qu.: 529.5     
 Max.   :4      Max.   :1           Max.   :1440.0     
 NA's   :11     NA's   :11          NA's   :11         
 registeredtimescore.day2 missingtime.day2 missingtimescore.day2
 Min.   :0.1014           Min.   :   0.0   Min.   :0.1014       
 1st Qu.:0.1380           1st Qu.: 910.5   1st Qu.:0.1380       
 Median :0.1601           Median :1209.5   Median :0.1601       
 Mean   :0.3436           Mean   : 945.2   Mean   :0.3436       
 3rd Qu.:0.3677           3rd Qu.:1241.2   3rd Qu.:0.3677       
 Max.   :1.0000           Max.   :1294.0   Max.   :1.0000       
 NA's   :11               NA's   :11       NA's   :11           
 numberofregistrations.day2 numberofsleep.day2 numberofeat.day2
 Min.   :1.0                Min.   : NA        Min.   : NA     
 1st Qu.:1.0                1st Qu.: NA        1st Qu.: NA     
 Median :1.5                Median : NA        Median : NA     
 Mean   :1.5                Mean   :NaN        Mean   :NaN     
 3rd Qu.:2.0                3rd Qu.: NA        3rd Qu.: NA     
 Max.   :2.0                Max.   : NA        Max.   : NA     
 NA's   :11                 NA's   :23         NA's   :23      
 qualitychecks.day2 qualityscore.day2 dstchange.day3 starthour.day3 
 Min.   :0.3333     Min.   :0.4009    Min.   :0      Min.   :4.000  
 1st Qu.:0.3333     1st Qu.:0.4253    1st Qu.:0      1st Qu.:4.000  
 Median :0.3333     Median :0.4400    Median :0      Median :4.000  
 Mean   :0.4444     Mean   :0.5624    Mean   :0      Mean   :5.417  
 3rd Qu.:0.3333     3rd Qu.:0.5785    3rd Qu.:0      3rd Qu.:6.125  
 Max.   :1.0000     Max.   :1.0000    Max.   :0      Max.   :8.250  
 NA's   :11         NA's   :11        NA's   :20     NA's   :20     
 starthourscore.day3 registeredtime.day3 registeredtimescore.day3
 Min.   :0.0000      Min.   : 10.0       Min.   :0.006944        
 1st Qu.:0.5000      1st Qu.:306.5       1st Qu.:0.212847        
 Median :1.0000      Median :603.0       Median :0.418750        
 Mean   :0.6667      Mean   :408.3       Mean   :0.283565        
 3rd Qu.:1.0000      3rd Qu.:607.5       3rd Qu.:0.421875        
 Max.   :1.0000      Max.   :612.0       Max.   :0.425000        
 NA's   :20          NA's   :20          NA's   :20              
 missingtime.day3 missingtimescore.day3 numberofregistrations.day3
 Min.   : 828.0   Min.   :0.006944      Min.   : 1.00             
 1st Qu.: 832.5   1st Qu.:0.212847      1st Qu.: 1.50             
 Median : 837.0   Median :0.418750      Median : 2.00             
 Mean   :1031.7   Mean   :0.283565      Mean   :22.33             
 3rd Qu.:1133.5   3rd Qu.:0.421875      3rd Qu.:33.00             
 Max.   :1430.0   Max.   :0.425000      Max.   :64.00             
 NA's   :20       NA's   :20            NA's   :20                
 numberofsleep.day3 numberofeat.day3 qualitychecks.day3 qualityscore.day3
 Min.   : NA        Min.   : NA      Min.   :0.0000     Min.   :0.2833   
 1st Qu.: NA        1st Qu.: NA      1st Qu.:0.1667     1st Qu.:0.3106   
 Median : NA        Median : NA      Median :0.3333     Median :0.3380   
 Mean   :NaN        Mean   :NaN      Mean   :0.2222     Mean   :0.4113   
 3rd Qu.: NA        3rd Qu.: NA      3rd Qu.:0.3333     3rd Qu.:0.4752   
 Max.   : NA        Max.   : NA      Max.   :0.3333     Max.   :0.6125   
 NA's   :23         NA's   :23       NA's   :20         NA's   :20       
