---
title: "Kognitív teszt"
author: "Ilse Laurijssen"
date: "11 May, 2022 12:30"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "11 May, 2022 12:00"
* cleaned data: "11 May, 2022 12:00"

## Response

* Total number of respondents: 17

* Respondents who started the diary: 
9

* Respondents who finished the research: 16

### Finished data states over time (cumulative)

![plot of chunk progress](figures/334-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/334-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                    | finished| busy|
|:-------------------|--------:|----:|
|Háztartási kérdőív  |       16|    1|
|Személyi kérdőív    |       16|    0|
|Napló               |       16|    0|
|Napló nap jellemzői |       16|    0|
|Kognitív kérdőív    |       16|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries


```
## [1] "The quality indicators number of days and number of complete days are not available, so stopping here (for now)"
```










