---
title: "PhD Survey 2022"
author: "Ilse Laurijssen"
date: "05 April, 2022 15:00"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "05 April, 2022 15:00"
* cleaned data: "05 April, 2022 15:00"

## Response

* Total number of respondents: 6

* Respondents who started the diary: 


* Respondents who finished the research: 6

### Finished data states over time (cumulative)

![plot of chunk progress](figures/329-progress-1.png)

### Activity last week

```
## Error in charToDate(x): character string is not in a standard unambiguous format
```

```
## Error in charToDate(x): character string is not in a standard unambiguous format
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/329-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                | finished| busy|
|:---------------|--------:|----:|
|Begin           |        6|    0|
|PhD survey 2022 |        1|    5|



## Background characteristics


```
## What is your gender?
```



|Var1 | Freq|
|:----|----:|
|Male |    1|


















