---
title: "PhD survey 2020"
author: "Ilse Laurijssen"
date: "10 December, 2020 21:03"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "10 December, 2020 21:01"
* cleaned data: "10 December, 2020 21:02"

## Response

* Total number of respondents: 1607

* Respondents who started the diary: 


* Respondents who finished the research: 729

### Finished data states over time (cumulative)

![plot of chunk progress](figures/212-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/212-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                         | finished| busy|
|:------------------------|--------:|----:|
|Read in PhD candidates   |     1607|    0|
|Invitation and reminders |      769|  838|
|PhD Questionnaire 2020   |      729|   40|
|Thank you page           |        0|  729|



## Background characteristics


```
## My (main) faculty is (select one):
```



|Var1                                     | Freq|
|:----------------------------------------|----:|
|Arts & Philosophy                        |   64|
|Social Sciences & Business Solvay School |  109|
|Engineering Sciences                     |  142|
|Law & Criminology                        |   31|
|Medicine & Pharmacy                      |  142|
|Psychology & Educational Sciences        |   48|
|Sciences & Bio-science Engineering       |  151|
|Physical Education & Physiotherapy       |   35|
|Interdisciplinary Doctorate              |    6|

```
## In what stage/phase is your PhD?
```



|Var1                                                                                  | Freq|
|:-------------------------------------------------------------------------------------|----:|
|Starting phase <br /> (developing your research plan and design, reading…)            |  150|
|Executing phase <br /> (working on experiments, data, executing research plan/method) |  378|
|Finalizing phase<br /> (writing up phase)                                             |  183|


















