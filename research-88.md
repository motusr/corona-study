---
title: "Leraren"
author: "Ilse Laurijssen"
date: "10 December, 2020 22:21"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "10 December, 2020 21:29"
* cleaned data: "10 December, 2020 22:21"

## Response

* Total number of respondents: 34223

* Respondents who started the diary: 
13950

* Respondents who finished the research: 9376

### Finished data states over time (cumulative)

![plot of chunk progress](figures/88-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/88-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                                               | finished| busy|
|:--------------------------------------------------------------|--------:|----:|
|State 0: Pauze om inschrijvingsmail te ontvangen               |    34223|    0|
|State 1: Uitnodigingen en herinneringen TLK18                  |    34223|    0|
|State 2: Profielvragenlijst TLK18                              |    24383|    0|
|State 3: Spreiding veldwerk TLK18                              |    21115|    0|
|State 4: Tussenpagina profiel & voorvragenlijst TLK18          |    17471| 3644|
|State 5: Voorvragenlijst TLK18                                 |    17471|    0|
|State 6: Tussenpagina voorvragenlijst & tijdsregistratie TLK18 |    16652|    4|
|State 7: Tijdsregistratie 7 dagen TLK18                        |    16652|    0|
|State 8: Tussenpagina tijdsregistratie & navragenlijst TLK18   |     9559|    0|
|State 9: Navragenlijst TLK18                                   |     9559|    0|
|State 10: Eindscherm onderzoek TLK18                           |     9376|    0|



## Background characteristics


```
## Bent u ... ?
```



|Var1          |  Freq|
|:-------------|-----:|
|Man           |  3886|
|Vrouw         | 17501|
|Geen antwoord |    17|

```
## Wat is het hoogste onderwijsniveau dat u met succes heeft beëindigd?
```



|Var1                                                              |  Freq|
|:-----------------------------------------------------------------|-----:|
|Geen diploma of lager onderwijs                                   |    23|
|Lager secundair onderwijs                                         |    73|
|Hoger secundair onderwijs                                         |   506|
|Hoger onderwijs korte type (bachelor of kandidaat)                | 11029|
|Hoger onderwijs lange type of universiteit (master of licentiaat) |  4865|
|Andere                                                            |   126|





## Quality indicators for diaries



### How many days and complete days in the diary (cumulative)?
Note: complete days as defined by criteria (maximum amount of missing time, minimum number of activities)

![plot of chunk diarydays](figures/88-diarydays-1.png)

Table: Data table: Number of days in diary

|numberofdays | total| complete| incomplete|
|:------------|-----:|--------:|----------:|
|1            | 13950|    10259|       3691|
|2            | 11846|     9731|       2115|
|3            | 11149|     9388|       1761|
|4            | 10673|     9090|       1583|
|5            | 10292|     8735|       1557|
|6            |  9988|     8193|       1795|
|7            |  9638|     7137|       2501|

### Number of respondents with at least one diary day
Different numbers, depending on exact criterium:

* Total days (complete + incomplete), minimum 2: 1.1846 &times; 10<sup>4</sup>
* Complete days, at least 1 complete day: 1.0259 &times; 10<sup>4</sup>
* Total registered hours at least 24h: 11616
* First day complete (based on criteria of maximum of undefined time, and minimum number of registrations): 9444

### Total number of  diary days
* Number of complete days: 6.2533 &times; 10<sup>4</sup>

### Time use results

```
## time use in minutes per day (on average)
```

```
## STRICT selection of respondents: 7 complete diary days
## Calculating base time parameters counting each resp 
##  for every value in  mainact_code_up1 and day_wkd
```


```
## averaged base time parameters (N=7137) 
##  for every value in  mainact_code_up1 and day_wkd
```

<img src="figures/88-timeuse-1.png" title="plot of chunk timeuse" alt="plot of chunk timeuse" width="100%" />

Table: Data table: Average durations per type of activity

|                                                             | weekday| weekendday|
|:------------------------------------------------------------|-------:|----------:|
|Lesvoorbereiding en verbeterwerk ...                         |   118.9|      119.8|
|Lesgeven en lesvervangende activiteiten ...                  |   184.0|        1.7|
|Leerlingenbegeleiding, contact met ouders  ...               |    13.0|        2.0|
|Professioneel overleg (intern en extern) ...                 |    30.9|        3.0|
|Opvang en toezicht  ...                                      |    18.3|        0.5|
|Klasadministratie, beheer materiaal en lokaal  ...           |    31.0|       18.4|
|Schoolorganisatie en beleidsondersteuning ...                |    14.2|        5.3|
|Vorming  ...                                                 |    12.3|        6.0|
|Andere werkactiviteiten ...                                  |    37.6|       24.3|
|Betaald werk buiten het onderwijs  ...                       |     3.5|        3.9|
|Verplaatsingen, wachten ...                                  |    71.9|       26.8|
|Persoonlijke verzorging, slapen, eten en drinken  ...        |   600.1|      681.5|
|Huishoudelijke taken, kinderzorg (privé) ...                 |   128.3|      196.4|
|Cultuur, vrije tijd, sociale participatie, media (privé) ... |   133.7|      253.7|
|Overige tijdsbesteding (privé) ...                           |    41.7|       96.5|
|NA ...                                                       |     0.6|        0.4|

### Time use patterns

Note: graphs below are experimental.
Peek around 12PM needs to be checked ...


```
## Calculating participation for each 10-min time slot of the day counting each resp 
##  for every value in  mainact_code_up1, day_wkd
```

```
## 0. add time variables
```

```
## 1. aggregate at individual level
```

```
## 2. add extra zeros
```

```
## Participation rates for each 10-min time slot of the day (N=7137) 
##  for every value in  mainact_code_up1 and day_wkd
```

```
## Participation rates for each 10-min time slot of the day (N=7137) 
##  for every value in  mainact_code_up1
```

```
## Participation rates for each 10-min time slot of the day (N=7137) 
##  for every value in  mainact_code_up1
```

<img src="figures/88-rythms-1.png" title="plot of chunk rythms" alt="plot of chunk rythms" width="100%" />
