---
title: "Feedback PhD survey 2019"
author: "Ilse Laurijssen"
date: "20 May, 2020 00:07"
output:
  pdf_document: default
  html_document: default
  word_document: default
---




```
## Warning in file(filename, "r", encoding = encoding): cannot open file
## 'test-knitr-params.R': No such file or directory
```

```
## Error in file(filename, "r", encoding = encoding): cannot open the connection
```

### Data

* raw SQL data: "20 May, 2020 00:07"
* cleaned data: "20 May, 2020 00:07"

## Respons

### Total number of respondents


```
## 713
```
### How many started the diary?

```
## Error in unique(diary$resp): object 'diary' not found
```

### How many finished the research?

```
## 713
```

### Finished data states over time (cumulative)


```
## Warning in min(x): no non-missing arguments to min; returning Inf
```

```
## Warning in max(x): no non-missing arguments to max; returning -Inf
```

```
## Warning in min(x): no non-missing arguments to min; returning Inf
```

```
## Warning in max(x): no non-missing arguments to max; returning -Inf
```

```
## Error in plot.window(...): need finite 'xlim' values
```

![plot of chunk progress](figures/225-progress-1.svg)

```
## Error in as.graphicsAnnot(labels): object 'timelabels' not found
```

```
## Error in as.graphicsAnnot(legend): argument "legend" is missing, with no default
```

### How many have passed (or get stuck in) each stage?

```
## Error in rs_resp_laststate$resp %in% diary$resp: object 'diary' not found
```

![plot of chunk stagescum](figures/225-stagescum-1.svg)

|                | finished| some data| busy|
|:---------------|--------:|---------:|----:|
|Inlezen         |      713|         0|    0|
|Send out e-mail |        0|         0|  713|



## Background characteristics


```
## 
## Use 'expss_output_viewer()' to display tables in the RStudio Viewer.
##  To return to the console output, use 'expss_output_default()'.
```

```
## Error in merge(survey, rs_resp_laststate, all.x = T, all.y = F, by = "resp"): object 'survey' not found
```

```
## Error in rmd_bgvar1 %in% names(flow_withsurvey): object 'flow_withsurvey' not found
```

```
## Error in rmd_bgvar2 %in% names(flow_withsurvey): object 'flow_withsurvey' not found
```

## Quality indicators for diaries

### How many days and complete days in the diary (cumulative)?
Note: complete days as defined by criteria (maximum amount of missing time, minimum number of activities)

```
## Error in table(survey$numberofdays): object 'survey' not found
```

```
## Error in table(survey$numberofcompletedays): object 'survey' not found
```

```
## Error in merge(numberofdays, numberofcompletedays, by = "row.names", all = T): object 'numberofdays' not found
```

```
## Error in names(numberofdays) = c("numberofdays", "total", "complete"): object 'numberofdays' not found
```

```
## Error in eval(expr, envir, enclos): object 'numberofdays' not found
```

```
## Error in eval(expr, envir, enclos): object 'numberofdays' not found
```

```
## Error in numberofdays[is.na(numberofdays)] = 0: object 'numberofdays' not found
```

```
## Error in t(numberofdays[, c("complete", "incomplete")]): object 'numberofdays' not found
```

```
## Error in knitr::kable(numberofdays, row.names = FALSE): object 'numberofdays' not found
```

### Number of respondents with at least one diary day

```
## total days (complete + incomplete), minimum 2
```

```
## Error in eval(expr, envir, enclos): object 'numberofdays' not found
```

```
## complete days, at least 1 complete day
```

```
## Error in eval(expr, envir, enclos): object 'numberofdays' not found
```

```
## total registered hours at least 24h
```

```
## Error in eval(expr, envir, enclos): object 'survey' not found
```

```
## first day complete, 
## based on criteria of maximum of undefined time, and minimum number of registrations
```

```
## Error in eval(expr, envir, enclos): object 'survey' not found
```
### Total number of  diary days

```
## complete days
```

```
## Error in eval(expr, envir, enclos): object 'numberofdays' not found
```

### Time use results

```
## time use in minutes per day (on average)
```

```
## no diary split per day present, so stopping here (for now)
```

```
## Error in eval(expr, envir, enclos): object 'diary' not found
```

```
## Error in eval(expr, envir, enclos): object 'diary_split' not found
```

```
## Error in summary(dataperday): object 'dataperday' not found
```

```
## Error in tapply(summaryperday$duration, list(summaryperday[[actgroupvar]], : object 'summaryperday' not found
```

```
## Error in factor(summaryperday$day_wkd%%7 < 2, labels = c("weekday", "weekendday")): object 'summaryperday' not found
```

```
## Error in eval(m$data, parent.frame()): object 'summaryperday' not found
```

```
## Error in tapply(dataperweekday$duration, list(dataperweekday[[actgroupvar]], : object 'dataperweekday' not found
```

```
## Error in barplot.default(tabel, col = kleuren): 'height' must be a vector or a matrix
```

```
## Error in strwidth(legend, units = "user", cex = cex, font = text.font): plot.new has not been called yet
```



|                    | finished| some data| busy|
|:-------------------|--------:|---------:|----:|
|Inlezen ...         |      713|         0|    0|
|Send out e-mail ... |        0|         0|  713|


