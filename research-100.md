---
title: "Test research HETUS"
author: "Ilse Laurijssen"
date: "28 December, 2023 19:00"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "28 December, 2023 19:00"
* cleaned data: "28 December, 2023 19:00"

## Response

* Total number of respondents: 79

* Respondents who started the diary: 
56

* Respondents who finished the research: 16

### Finished data states over time (cumulative)

![plot of chunk progress](figures/100-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/100-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                  | finished| busy|
|:-----------------|--------:|----:|
|Pause to start    |       41|    0|
|Communication     |       27|   14|
|Pre-questionnaire |       22|    5|
|HETUS diary       |       18|    4|
|Post-diary        |       15|    1|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries


```
## [1] "The quality indicators number of days and number of complete days are not available, so stopping here (for now)"
```










