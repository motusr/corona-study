---
title: "HETUS SOURCE TM - DESTATIS"
author: "Ilse Laurijssen"
date: "28 December, 2023 19:00"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "28 December, 2023 19:00"
* cleaned data: "28 December, 2023 19:00"

## Response

* Total number of respondents: 18

* Respondents who started the diary: 
13

* Respondents who finished the research: 3

### Finished data states over time (cumulative)

![plot of chunk progress](figures/229-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/229-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                       | finished| busy|
|:--------------------------------------|--------:|----:|
|READ IN PHASE                          |       18|    0|
|LOGIN                                  |       18|    0|
|Task 1: Household questionnaire        |       16|    0|
|Task 2: Individual questionnaire       |       13|    0|
|Task 3: Time diary                     |       13|    0|
|Task 4: End of day questionnaire       |       12|    0|
|Communication evaluation questionnaire |        7|    0|
|Task 5: Evaluation questionnaire       |        6|    0|
|Thank you page                         |        3|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries


```
## [1] "The quality indicators number of days and number of complete days are not available, so stopping here (for now)"
```










