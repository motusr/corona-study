---
title: "Prio Climat - Fraternelle 5"
author: "Ilse Laurijssen"
date: "18 April, 2022 18:30"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "18 April, 2022 18:30"
* cleaned data: "18 April, 2022 18:30"

## Response

* Total number of respondents: 3

* Respondents who started the diary: 


* Respondents who finished the research: 1

### Finished data states over time (cumulative)

![plot of chunk progress](figures/246-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/246-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                   | finished| busy|
|:------------------|--------:|----:|
|INVITATION EMAIL   |        3|    0|
|Survey no feedback |        3|    0|
|Phase 1 completed  |        2|    1|
|Survey feedback    |        1|    1|
|PHASE 2 completed  |        0|    1|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```


















