---
title: "TEST 1 - UN Women TUS"
author: "Ilse Laurijssen"
date: "15 November, 2021 13:00"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "15 November, 2021 13:00"
* cleaned data: "15 November, 2021 13:00"

## Response

* Total number of respondents: 24

* Respondents who started the diary: 
6

* Respondents who finished the research: 0

### Finished data states over time (cumulative)

![plot of chunk progress](figures/290-progress-1.png)

### Activity last week
![plot of chunk lastdays](figures/290-lastdays-1.png)

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/290-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                         | finished| busy|
|:------------------------|--------:|----:|
|READ IN PARTICIPANTS     |       23|    0|
|SEND INVITATION          |       23|    0|
|INDIVIDUAL QUESTIONNAIRE |       16|    0|
|WAIT TO START TIME DIARY |       15|    0|
|7 DAY TIME DIARY         |       10|    5|
|SEND THANK YOU EMAIL     |        0|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries


```
## [1] "The quality indicators number of days and number of complete days are not available, so stopping here (for now)"
```










