      resp          dstchange        starthour starthourscore registeredtime 
 Min.   : 68030   Min.   :-60.00   Min.   :4   Min.   :1      Min.   : 9195  
 1st Qu.: 68035   1st Qu.:  0.00   1st Qu.:4   1st Qu.:1      1st Qu.: 9975  
 Median : 68042   Median :  0.00   Median :4   Median :1      Median :10080  
 Mean   : 72721   Mean   :-10.59   Mean   :4   Mean   :1      Mean   : 9980  
 3rd Qu.: 68046   3rd Qu.:  0.00   3rd Qu.:4   3rd Qu.:1      3rd Qu.:10080  
 Max.   :146706   Max.   :  0.00   Max.   :4   Max.   :1      Max.   :10080  
 registeredtimescore  missingtime     missingtimescore numberofregistrations
 Min.   :0.9122      Min.   :  0.00   Min.   :0.9122   Min.   : 65.0        
 1st Qu.:0.9896      1st Qu.:  0.00   1st Qu.:0.9896   1st Qu.: 91.0        
 Median :1.0000      Median :  0.00   Median :1.0000   Median :103.0        
 Mean   :0.9901      Mean   : 99.76   Mean   :0.9901   Mean   :105.6        
 3rd Qu.:1.0000      3rd Qu.:105.00   3rd Qu.:1.0000   3rd Qu.:123.0        
 Max.   :1.0000      Max.   :885.00   Max.   :1.0000   Max.   :144.0        
 numberofsleep  numberofeat    qualitychecks     qualityscore   
 Min.   : 8    Min.   :11.00   Min.   :0.3333   Min.   :0.9415  
 1st Qu.: 8    1st Qu.:15.00   1st Qu.:0.3333   1st Qu.:0.9931  
 Median : 8    Median :20.00   Median :1.0000   Median :1.0000  
 Mean   : 9    Mean   :18.88   Mean   :0.7255   Mean   :0.9934  
 3rd Qu.: 9    3rd Qu.:21.00   3rd Qu.:1.0000   3rd Qu.:1.0000  
 Max.   :14    Max.   :25.00   Max.   :1.0000   Max.   :1.0000  
 less_50_registrations more_14h_missingtime
 Mode :logical         Mode :logical       
 FALSE:17              FALSE:16            
                       TRUE :1             
                                           
                                           
                                           
