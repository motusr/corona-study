---
title: "VELUX RA - Agenda - Hiver 2019"
author: "Ilse Laurijssen"
date: "10 December, 2020 21:03"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "10 December, 2020 21:01"
* cleaned data: "10 December, 2020 21:02"

## Response

* Total number of respondents: 3

* Respondents who started the diary: 
2

* Respondents who finished the research: 3

### Finished data states over time (cumulative)


```
## 
## 
## |           | Noname|
## |:----------|------:|
## |2019-04-14 |      2|
```

### Activity last week


### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/187-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|       | finished| busy|
|:------|--------:|----:|
|Noname |        2|    1|



## Background characteristics




















