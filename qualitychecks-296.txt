      resp          dstchange   starthour     starthourscore registeredtime
 Min.   :169842   Min.   :0   Min.   : 4.00   Min.   :0.0    Min.   :  10  
 1st Qu.:171691   1st Qu.:0   1st Qu.: 4.00   1st Qu.:0.0    1st Qu.:1478  
 Median :172828   Median :0   Median : 4.00   Median :1.0    Median :2810  
 Mean   :172480   Mean   :0   Mean   : 6.25   Mean   :0.6    Mean   :2102  
 3rd Qu.:172984   3rd Qu.:0   3rd Qu.: 6.25   3rd Qu.:1.0    3rd Qu.:3008  
 Max.   :174254   Max.   :0   Max.   :13.00   Max.   :1.0    Max.   :3250  
                  NA's   :2   NA's   :2                                    
 registeredtimescore  missingtime     missingtimescore   numberofregistrations
 Min.   :0.003472    Min.   :-190.0   Min.   :0.003472   Min.   : 1.00        
 1st Qu.:0.513021    1st Qu.: -41.0   1st Qu.:0.502604   1st Qu.: 4.50        
 Median :0.975694    Median : 725.0   Median :0.748264   Median : 8.50        
 Mean   :0.704167    Mean   : 960.2   Mean   :0.654167   Mean   :11.20        
 3rd Qu.:1.000000    3rd Qu.:1432.5   3rd Qu.:0.993924   3rd Qu.:18.75        
 Max.   :1.000000    Max.   :2870.0   Max.   :1.000000   Max.   :24.00        
                                                                              
 numberofsleep  numberofeat  qualitychecks     qualityscore     numberofdays 
 Min.   : NA   Min.   : NA   Min.   :0.0000   Min.   :0.3356   Min.   :1.00  
 1st Qu.: NA   1st Qu.: NA   1st Qu.:0.3333   1st Qu.:0.6505   1st Qu.:2.00  
 Median : NA   Median : NA   Median :0.3333   Median :0.6667   Median :2.00  
 Mean   :NaN   Mean   :NaN   Mean   :0.4333   Mean   :0.6528   Mean   :2.10  
 3rd Qu.: NA   3rd Qu.: NA   3rd Qu.:0.6667   3rd Qu.:0.6788   3rd Qu.:2.75  
 Max.   : NA   Max.   : NA   Max.   :1.0000   Max.   :1.0000   Max.   :3.00  
 NA's   :10    NA's   :10                                                    
      resp        dstchange.day1 starthour.day1 starthourscore.day1
 Min.   :169842   Min.   :0      Min.   : 4     Min.   :0.00       
 1st Qu.:171691   1st Qu.:0      1st Qu.: 4     1st Qu.:0.25       
 Median :172828   Median :0      Median : 4     Median :1.00       
 Mean   :172480   Mean   :0      Mean   : 6     Mean   :0.70       
 3rd Qu.:172984   3rd Qu.:0      3rd Qu.: 4     3rd Qu.:1.00       
 Max.   :174254   Max.   :0      Max.   :13     Max.   :1.00       
                  NA's   :1      NA's   :1                         
 registeredtime.day1 registeredtimescore.day1 missingtime.day1
 Min.   :  10        Min.   :0.006944         Min.   :   0    
 1st Qu.: 900        1st Qu.:0.625000         1st Qu.:   0    
 Median :1405        Median :0.975694         Median :  35    
 Mean   :1054        Mean   :0.731944         Mean   : 386    
 3rd Qu.:1440        3rd Qu.:1.000000         3rd Qu.: 540    
 Max.   :1440        Max.   :1.000000         Max.   :1430    
                                                              
 missingtimescore.day1 numberofregistrations.day1 numberofsleep.day1
 Min.   :0.006944      Min.   : 1.00              Min.   : NA       
 1st Qu.:0.625000      1st Qu.: 2.50              1st Qu.: NA       
 Median :0.975694      Median : 4.50              Median : NA       
 Mean   :0.731944      Mean   : 6.20              Mean   :NaN       
 3rd Qu.:1.000000      3rd Qu.: 9.75              3rd Qu.: NA       
 Max.   :1.000000      Max.   :16.00              Max.   : NA       
                                                  NA's   :10        
 numberofeat.day1 qualitychecks.day1 qualityscore.day1 dstchange.day2
 Min.   : NA      Min.   :0.00000    Min.   :0.3380    Min.   :0     
 1st Qu.: NA      1st Qu.:0.08333    1st Qu.:0.4167    1st Qu.:0     
 Median : NA      Median :0.66667    Median :0.8171    Median :0     
 Mean   :NaN      Mean   :0.56667    Mean   :0.7213    Mean   :0     
 3rd Qu.: NA      3rd Qu.:1.00000    3rd Qu.:1.0000    3rd Qu.:0     
 Max.   : NA      Max.   :1.00000    Max.   :1.0000    Max.   :0     
 NA's   :10                                            NA's   :5     
 starthour.day2 starthourscore.day2 registeredtime.day2
 Min.   :4      Min.   :0.0000      Min.   :  30.0     
 1st Qu.:4      1st Qu.:1.0000      1st Qu.: 387.5     
 Median :4      Median :1.0000      Median :1444.0     
 Mean   :4      Mean   :0.8333      Mean   :1023.0     
 3rd Qu.:4      3rd Qu.:1.0000      3rd Qu.:1527.0     
 Max.   :4      Max.   :1.0000      Max.   :1630.0     
 NA's   :5      NA's   :4           NA's   :4          
 registeredtimescore.day2 missingtime.day2 missingtimescore.day2
 Min.   :0.02083          Min.   :-190     Min.   :0.02083      
 1st Qu.:0.26910          1st Qu.: -87     1st Qu.:0.26910      
 Median :0.97569          Median :  -4     Median :0.97569      
 Mean   :0.66898          Mean   : 417     Mean   :0.66898      
 3rd Qu.:1.00000          3rd Qu.:1052     3rd Qu.:1.00000      
 Max.   :1.00000          Max.   :1410     Max.   :1.00000      
 NA's   :4                NA's   :4        NA's   :4            
 numberofregistrations.day2 numberofsleep.day2 numberofeat.day2
 Min.   : 1.000             Min.   : NA        Min.   : NA     
 1st Qu.: 3.500             1st Qu.: NA        1st Qu.: NA     
 Median : 5.000             Median : NA        Median : NA     
 Mean   : 5.833             Mean   :NaN        Mean   :NaN     
 3rd Qu.: 8.000             3rd Qu.: NA        3rd Qu.: NA     
 Max.   :12.000             Max.   : NA        Max.   : NA     
 NA's   :4                  NA's   :10         NA's   :10      
 qualitychecks.day2 qualityscore.day2 dstchange.day3 starthour.day3
 Min.   :0.0000     Min.   :0.3472    Min.   :0      Min.   :4     
 1st Qu.:0.3333     1st Qu.:0.4294    1st Qu.:0      1st Qu.:4     
 Median :0.6667     Median :0.8171    Median :0      Median :4     
 Mean   :0.6111     Mean   :0.7238    Mean   :0      Mean   :4     
 3rd Qu.:1.0000     3rd Qu.:1.0000    3rd Qu.:0      3rd Qu.:4     
 Max.   :1.0000     Max.   :1.0000    Max.   :0      Max.   :4     
 NA's   :4          NA's   :4         NA's   :5      NA's   :5     
 starthourscore.day3 registeredtime.day3 registeredtimescore.day3
 Min.   :1           Min.   : 125        Min.   :0.08681         
 1st Qu.:1           1st Qu.: 620        1st Qu.:0.43056         
 Median :1           Median : 720        Median :0.50000         
 Mean   :1           Mean   : 869        Mean   :0.60347         
 3rd Qu.:1           3rd Qu.:1440        3rd Qu.:1.00000         
 Max.   :1           Max.   :1440        Max.   :1.00000         
 NA's   :5           NA's   :5           NA's   :5               
 missingtime.day3 missingtimescore.day3 numberofregistrations.day3
 Min.   :   0     Min.   :0.08681       Min.   : 1.0              
 1st Qu.:   0     1st Qu.:0.43056       1st Qu.: 2.0              
 Median : 720     Median :0.50000       Median : 2.0              
 Mean   : 571     Mean   :0.60347       Mean   : 4.4              
 3rd Qu.: 820     3rd Qu.:1.00000       3rd Qu.: 5.0              
 Max.   :1315     Max.   :1.00000       Max.   :12.0              
 NA's   :5        NA's   :5             NA's   :5                 
 numberofsleep.day3 numberofeat.day3 qualitychecks.day3 qualityscore.day3
 Min.   : NA        Min.   : NA      Min.   :0.3333     Min.   :0.3912   
 1st Qu.: NA        1st Qu.: NA      1st Qu.:0.3333     1st Qu.:0.6204   
 Median : NA        Median : NA      Median :0.3333     Median :0.6667   
 Mean   :NaN        Mean   :NaN      Mean   :0.6000     Mean   :0.7356   
 3rd Qu.: NA        3rd Qu.: NA      3rd Qu.:1.0000     3rd Qu.:1.0000   
 Max.   : NA        Max.   : NA      Max.   :1.0000     Max.   :1.0000   
 NA's   :10         NA's   :10       NA's   :5          NA's   :5        
