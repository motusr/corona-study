---
title: "TEST 2 - UN Women TUS"
author: "Ilse Laurijssen"
date: "12 July, 2023 09:30"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "12 July, 2023 09:30"
* cleaned data: "12 July, 2023 09:30"

## Response

* Total number of respondents: 6

* Respondents who started the diary: 
2

* Respondents who finished the research: 0

### Finished data states over time (cumulative)

![plot of chunk progress](figures/294-progress-1.png)

### Activity last week
![plot of chunk lastdays](figures/294-lastdays-1.png)

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/294-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                         | finished| busy|
|:------------------------|--------:|----:|
|READ IN PARTICIPANTS     |        5|    0|
|SEND INVITATION          |        5|    0|
|INDIVIDUAL QUESTIONNAIRE |        5|    0|
|WAIT TO START TIME DIARY |        3|    0|
|7 DAY TIME DIARY         |        3|    0|
|SEND THANK YOU EMAIL     |        0|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries


```
## [1] "The quality indicators number of days and number of complete days are not available, so stopping here (for now)"
```










