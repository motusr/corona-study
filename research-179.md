---
title: "Femma meting 3 partner"
author: "Ilse Laurijssen"
date: "10 December, 2020 21:03"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "10 December, 2020 21:01"
* cleaned data: "10 December, 2020 21:02"

## Response

* Total number of respondents: 24

* Respondents who started the diary: 
17

* Respondents who finished the research: 16

### Finished data states over time (cumulative)

![plot of chunk progress](figures/179-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/179-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                                   | finished| busy|
|:--------------------------------------------------|--------:|----:|
|State 1: aankondiging meting 1                     |       19|    5|
|STATE 2: TUSSENPAGINA START VOORVRAGENLIJST        |       18|    1|
|STATE 3: VOORVRAGENLIJST                           |       18|    0|
|STATE 4: TUSSENPAGINA VOORVRAGENLIJST EN DAGBOEKJE |       18|    0|
|STATE 5: TIJDSGREGISTRATIE                         |       18|    0|
|STATE 6: TUSSENPAGINA DAGBOEKJE EN NAVRAGENLIJST   |       16|    2|
|STATE 7: NAVRAGENLIJST                             |       16|    0|
|EINDSCHERM ONDERZOEK METING 1                      |        0|   16|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries


```
## [1] "The quality indicators number of days and number of complete days are not available, so stopping here (for now)"
```










