---
title: "Movement"
author: "Ilse Laurijssen"
date: "02 November, 2022 16:00"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "02 November, 2022 16:00"
* cleaned data: "02 November, 2022 16:00"

## Response

* Total number of respondents: 1

* Respondents who started the diary: 


* Respondents who finished the research: 1

### Finished data states over time (cumulative)


```
## No data available for finished data states
```

### Activity last week


### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/331-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                    | finished| busy|
|:-------------------|--------:|----:|
|Dagelijkse beweging |        0|    1|



## Background characteristics




















