---
title: "BRUXODUS G2 EN"
author: "Ilse Laurijssen"
date: "10 December, 2020 21:03"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "10 December, 2020 21:00"
* cleaned data: "10 December, 2020 21:01"

## Response

* Total number of respondents: 1

* Respondents who started the diary: 


* Respondents who finished the research: 1

### Finished data states over time (cumulative)


```
## 
## 
## |           | Surveystate|
## |:----------|-----------:|
## |2018-02-28 |           1|
```

### Activity last week


### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/122-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                  | finished| busy|
|:-----------------|--------:|----:|
|Uitnodigingsstate |        1|    0|
|Surveystate       |        1|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```


















