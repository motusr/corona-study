---
title: ""
author: "Ilse Laurijssen"
date: "04 September, 2020 16:06"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "04 September, 2020 16:06"
* cleaned data: "04 September, 2020 16:06"

## Response

* Total number of respondents: 75




* Respondents who finished the research: 74

### Finished data states over time (cumulative)

![plot of chunk progress](figures/226-progress-1.svg)

### Activity last week
![plot of chunk lastdays](figures/226-lastdays-1.svg)

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/226-stagescum-1.svg)

|           | finished| busy|
|:----------|--------:|----:|
|Bib A      |       74|    1|
|Bib B      |       74|    0|
|Bib C      |       74|    0|
|Bib D      |       74|    0|
|Bib E      |       74|    0|
|Afsluitend |       74|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```


















