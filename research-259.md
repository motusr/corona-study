---
title: "TEstPim"
author: "Ilse Laurijssen"
date: "28 December, 2023 19:00"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "28 December, 2023 19:00"
* cleaned data: "28 December, 2023 19:00"

## Response

* Total number of respondents: 2

* Respondents who started the diary: 


* Respondents who finished the research: 2

### Finished data states over time (cumulative)


```
## No data available for finished data states
```

### Activity last week


### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/259-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|       | finished| busy|
|:------|--------:|----:|
|Noname |        0|    2|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```


















