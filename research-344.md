---
title: "DIGITUS"
author: "Ilse Laurijssen"
date: "14 June, 2023 10:30"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "14 June, 2023 10:30"
* cleaned data: "14 June, 2023 10:30"

## Response

* Total number of respondents: 551

* Respondents who started the diary: 
52

* Respondents who finished the research: 69

### Finished data states over time (cumulative)

![plot of chunk progress](figures/344-progress-1.png)

### Activity last week
![plot of chunk lastdays](figures/344-lastdays-1.png)

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/344-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                      | finished| busy|
|:---------------------|--------:|----:|
|Voorvragenlijst       |      204|  329|
|Pause tussen VV en D1 |      203|    1|
|Start Dagboekje       |       51|  152|
|Dagboekje             |       34|   17|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries


```
## [1] "The quality indicators number of days and number of complete days are not available, so stopping here (for now)"
```










