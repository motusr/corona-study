---
title: "TUS0 HETUS ESSnet TESTaccounts"
author: "Ilse Laurijssen"
date: "28 December, 2023 19:00"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "28 December, 2023 19:00"
* cleaned data: "28 December, 2023 19:00"

## Response

* Total number of respondents: 14

* Respondents who started the diary: 
5

* Respondents who finished the research: 0

### Finished data states over time (cumulative)

![plot of chunk progress](figures/254-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/254-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                       | finished| busy|
|:--------------------------------------|--------:|----:|
|READ IN PHASE                          |       14|    0|
|LOGIN                                  |       14|    0|
|Task 1: Household questionnaire        |       12|    0|
|Task 2: Individual questionnaire       |       10|    0|
|Task 3: Time diary                     |        9|    0|
|Task 4: End of day questionnaire       |        9|    0|
|Communication evaluation questionnaire |        0|    0|
|Task 5: Evaluation questionnaire       |        0|    0|
|Thank you page                         |        0|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries


```
## [1] "The quality indicators number of days and number of complete days are not available, so stopping here (for now)"
```










