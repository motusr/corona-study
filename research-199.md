---
title: "Femma meting 4 partner"
author: "Ilse Laurijssen"
date: "22 February, 2021 21:06"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "22 februari, 2021 21:06"
* cleaned data: "22 februari, 2021 21:06"

## Response

* Total number of respondents: 22

* Respondents who started the diary: 
14

* Respondents who finished the research: 13

### Finished data states over time (cumulative)

![plot of chunk progress](figures/199-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/199-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                                   | finished| busy|
|:--------------------------------------------------|--------:|----:|
|State 1: aankondiging meting 1                     |       15|    6|
|STATE 2: TUSSENPAGINA START VOORVRAGENLIJST        |       14|    1|
|STATE 3: VOORVRAGENLIJST                           |       14|    0|
|STATE 4: TUSSENPAGINA VOORVRAGENLIJST EN DAGBOEKJE |       14|    0|
|STATE 4.B: BEN JE KLAAR VOOR HET DAGBOEKJE?        |       13|    1|
|STATE 5: TIJDSGREGISTRATIE                         |       13|    0|
|STATE 6: TUSSENPAGINA DAGBOEKJE EN NAVRAGENLIJST   |       13|    0|
|STATE 7: NAVRAGENLIJST                             |       13|    0|
|EINDSCHERM ONDERZOEK METING 1                      |        0|   13|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries



### How many days and complete days in the diary (cumulative)?
Note: complete days as defined by criteria (maximum amount of missing time, minimum number of activities)

![plot of chunk diarydays](figures/199-diarydays-1.png)

Table: Data table: Number of days in diary

|numberofdays | total| complete| incomplete|
|:------------|-----:|--------:|----------:|
|1            |    14|       14|          0|
|2            |    14|       14|          0|
|3            |    14|       13|          1|
|4            |    13|       13|          0|
|5            |    13|       13|          0|
|6            |    13|       13|          0|
|7            |    13|       12|          1|

### Number of respondents with at least one diary day
Different numbers, depending on exact criterium:

* Total days (complete + incomplete), minimum 2: 14
* Complete days, at least 1 complete day: 14
* Total registered hours at least 24h: 14
* First day complete (based on criteria of maximum of undefined time, and minimum number of registrations): 14

### Total number of  diary days
* Number of complete days: 92

### Time use results

```
## time use in minutes per day (on average)
```

```
## BASED on selection of COMPLETE DAYS
## Calculating base time parameters counting each resp 
##  for every value in  mainact_code_up2
```


```
## averaged base time parameters (N=14) 
##  for every value in  mainact_code_up2
```

<img src="figures/199-timeuse-1.png" title="plot of chunk timeuse" alt="plot of chunk timeuse" width="100%" />

Table: Data table: Average durations per type of activity

|                                                           | weekday| weekendday|
|:----------------------------------------------------------|-------:|----------:|
|Betaald werk ...                                           |   285.5|       44.7|
|Huishoudelijk werk, klussen, winkelen en dienstbezoek ...  |   140.5|      154.2|
|Kinderzorg en hulp aan volwassenen (familie en andere) ... |    31.1|       57.4|
|Persoonlijke verzorging, eten en drinken ...               |   133.7|      145.0|
|Slapen, rusten en andere persoonlijke behoeften ...        |   476.5|      562.0|
|Opleiding ...                                              |     4.7|        0.0|
|Sociale contacten en verenigingsleven ...                  |    68.7|      135.3|
|Vrije tijd en media ...                                    |   195.8|      283.8|
|Wachten ...                                                |     2.8|        0.8|
|Verplaatsingen ...                                         |    88.0|       45.6|
|Overige ...                                                |    12.8|       11.1|
|NA ...                                                     |      NA|         NA|

### Time use patterns

Note: graphs below are experimental.
Peek around 12PM needs to be checked ...


```
## Calculating participation for each 10-min time slot of the day counting each resp 
##  for every value in  mainact_code_up2, day_wkd
```

```
## 0. add time variables
```

```
## 1. aggregate at individual level
```

```
## 2. add extra zeros
```

```
## Participation rates for each 10-min time slot of the day (N=14) 
##  for every value in  mainact_code_up2 and day_wkd
```

```
## Participation rates for each 10-min time slot of the day (N=14) 
##  for every value in  mainact_code_up2
```

```
## Participation rates for each 10-min time slot of the day (N=14) 
##  for every value in  mainact_code_up2
```

<img src="figures/199-rythms-1.png" title="plot of chunk rythms" alt="plot of chunk rythms" width="100%" />
