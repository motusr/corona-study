---
title: "BRUXODUS G2 FR"
author: "Ilse Laurijssen"
date: "10 December, 2020 21:03"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "10 December, 2020 21:00"
* cleaned data: "10 December, 2020 21:01"

## Response

* Total number of respondents: 14

* Respondents who started the diary: 


* Respondents who finished the research: 11

### Finished data states over time (cumulative)

![plot of chunk progress](figures/119-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/119-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                  | finished| busy|
|:-----------------|--------:|----:|
|Uitnodigingsstate |       11|    3|
|Surveystate       |        8|    3|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```


















