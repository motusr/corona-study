---
title: "PRÓBA TESZT"
author: "Ilse Laurijssen"
date: "11 May, 2022 12:01"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "11 May, 2022 12:00"
* cleaned data: "11 May, 2022 12:00"

## Response

* Total number of respondents: 5

* Respondents who started the diary: 
2

* Respondents who finished the research: 1

### Finished data states over time (cumulative)

![plot of chunk progress](figures/324-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/324-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                              | finished| busy|
|:-----------------------------|--------:|----:|
|Háztartási kérdőív            |        2|    1|
|Személyi kérdőív              |        2|    0|
|Első napló nap                |        1|    1|
|Az első napló nap jellemzői   |        1|    0|
|Második napló nap             |        1|    0|
|A második napló nap jellemzői |        1|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries


```
## [1] "The quality indicators number of days and number of complete days are not available, so stopping here (for now)"
```










