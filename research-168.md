---
title: "TVPK19 - AUVB"
author: "Ilse Laurijssen"
date: "10 December, 2020 21:03"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "10 December, 2020 21:00"
* cleaned data: "10 December, 2020 21:02"

## Response

* Total number of respondents: 607

* Respondents who started the diary: 
94

* Respondents who finished the research: 10

### Finished data states over time (cumulative)

![plot of chunk progress](figures/168-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/168-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                                              | finished| busy|
|:-------------------------------------------------------------|--------:|----:|
|STATE 1: Nieuwe respondent (registratie)                      |      586|   21|
|State 2: Voorvragenlijst TVPK19                               |      586|    0|
|State 3: Spreiding veldwerk TLK18                             |      324|    0|
|State 4: Tussenpagina  tijdsregistratie TVPK19                |      324|    0|
|State 5: Tijdsregistratie 7 dagen TPVK19                      |      324|    0|
|State 6: Tussenpagina tijdsregistratie & navragenlijst TVPK19 |       10|    0|
|State 7: Navragenlijst TVPK19                                 |       10|    0|
|State 8: Eindscherm onderzoek TLK18                           |       10|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries


```
## [1] "The quality indicators number of days and number of complete days are not available, so stopping here (for now)"
```










