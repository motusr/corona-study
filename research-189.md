---
title: "Forum Vies Mobiles Interviewees"
author: "Ilse Laurijssen"
date: "10 December, 2020 21:03"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "10 December, 2020 21:01"
* cleaned data: "10 December, 2020 21:02"

## Response

* Total number of respondents: 27

* Respondents who started the diary: 
14

* Respondents who finished the research: 8

### Finished data states over time (cumulative)

![plot of chunk progress](figures/189-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/189-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                               | finished| busy|
|:------------------------------|--------:|----:|
|Read in phase                  |       27|    0|
|Login                          |       27|    0|
|SURVEY: INITIAL QUESTIONNAIRE  |       15|    0|
|Diary and geofencing           |       15|    0|
|SURVEY: after two weeks survey |        8|    0|
|Geofencing                     |        8|    0|
|Research complete              |        8|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries


```
## [1] "The quality indicators number of days and number of complete days are not available, so stopping here (for now)"
```










