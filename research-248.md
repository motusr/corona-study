---
title: "Destatis - entire flow"
author: "Ilse Laurijssen"
date: "28 December, 2023 19:00"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "28 December, 2023 19:00"
* cleaned data: "28 December, 2023 19:00"

## Response

* Total number of respondents: 111

* Respondents who started the diary: 
45

* Respondents who finished the research: 56

### Finished data states over time (cumulative)

![plot of chunk progress](figures/248-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/248-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                | finished| busy|
|:-------------------------------|--------:|----:|
|READ IN PHASE                   |      111|    0|
|LOGIN                           |      111|    0|
|Task 1: Household questionnaire |       63|    0|
|Task 2: Labour Survey           |       58|    0|
|Task 3: Time diary              |       56|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries


```
## [1] "The quality indicators number of days and number of complete days are not available, so stopping here (for now)"
```










