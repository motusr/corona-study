---
title: "BIB2019"
author: "Ilse Laurijssen"
date: "10 December, 2020 21:03"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "10 December, 2020 21:02"
* cleaned data: "10 December, 2020 21:03"

## Response

* Total number of respondents: 10235

* Respondents who started the diary: 


* Respondents who finished the research: 3357

### Finished data states over time (cumulative)

![plot of chunk progress](figures/161-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/161-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|            | finished| busy|
|:-----------|--------:|----:|
|Vragenlijst |     3357| 6878|
|Einde       |        0| 3357|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```


















