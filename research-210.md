---
title: "Femma actieonderzoek WN mrt 20"
author: "Ilse Laurijssen"
date: "10 December, 2020 21:03"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "10 December, 2020 21:01"
* cleaned data: "10 December, 2020 21:03"

## Response

* Total number of respondents: 51

* Respondents who started the diary: 
43

* Respondents who finished the research: 43

### Finished data states over time (cumulative)

![plot of chunk progress](figures/210-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/210-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                                   | finished| busy|
|:--------------------------------------------------|--------:|----:|
|STATE 1: AANKONDIGING METING 1                     |       46|    5|
|STATE 2: TUSSENPAGINA START VOORVRAGENLIJST        |       46|    0|
|STATE 3: VOORVRAGENLIJST                           |       45|    1|
|State 4: Tussenpagina voorvragenlijst en dagboekje |       45|    0|
|STATE 5: TIJDSGREGISTRATIE                         |       45|    0|
|STATE 6: TUSSENPAGINA DAGBOEKJE EN NAVRAGENLIJST   |       45|    0|
|STATE 7: NAVRAGENLIJST                             |       43|    2|
|EINDSCHERM ONDERZOEK METING 1                      |        0|   43|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries



### How many days and complete days in the diary (cumulative)?
Note: complete days as defined by criteria (maximum amount of missing time, minimum number of activities)

![plot of chunk diarydays](figures/210-diarydays-1.png)

Table: Data table: Number of days in diary

|numberofdays | total| complete| incomplete|
|:------------|-----:|--------:|----------:|
|1            |    43|       39|          4|
|2            |    39|       39|          0|
|3            |    39|       38|          1|
|4            |    38|       37|          1|
|5            |    38|       36|          2|
|6            |    38|       36|          2|
|7            |    38|       34|          4|

### Number of respondents with at least one diary day
Different numbers, depending on exact criterium:

* Total days (complete + incomplete), minimum 2: 39
* Complete days, at least 1 complete day: 39
* Total registered hours at least 24h: 39
* First day complete (based on criteria of maximum of undefined time, and minimum number of registrations): 38

### Total number of  diary days
* Number of complete days: 259

### Time use results

```
## time use in minutes per day (on average)
```

```
## BASED on selection of COMPLETE DAYS
## Calculating base time parameters counting each resp 
##  for every value in  mainact_code_up2
```


```
## averaged base time parameters (N=43) 
##  for every value in  mainact_code_up2
```

<img src="figures/210-timeuse-1.png" title="plot of chunk timeuse" alt="plot of chunk timeuse" width="100%" />

Table: Data table: Average durations per type of activity

|                                                           | weekday| weekendday|
|:----------------------------------------------------------|-------:|----------:|
|Betaald werk ...                                           |   319.9|       24.3|
|Huishoudelijk werk, klussen, winkelen en dienstbezoek ...  |    93.4|      177.1|
|Kinderzorg en hulp aan volwassenen (familie en andere) ... |    33.3|       34.0|
|Persoonlijke verzorging, eten en drinken ...               |   115.1|      132.1|
|Slapen, rusten en andere persoonlijke behoeften ...        |   415.3|      461.6|
|Opleiding ...                                              |     3.5|        3.5|
|Sociale contacten en verenigingsleven ...                  |    40.9|       78.5|
|Vrije tijd en media ...                                    |   135.5|      234.6|
|Wachten ...                                                |     2.0|        0.0|
|Onderweg ...                                               |    88.0|       65.0|
|Overige ...                                                |    29.1|       41.9|
|NA ...                                                     |   148.1|      174.2|

### Time use patterns

Note: graphs below are experimental.
Peek around 12PM needs to be checked ...


```
## Calculating participation for each 10-min time slot of the day counting each resp 
##  for every value in  mainact_code_up2, day_wkd
```

```
## 0. add time variables
```

```
## 1. aggregate at individual level
```

```
## 2. add extra zeros
```

```
## Participation rates for each 10-min time slot of the day (N=43) 
##  for every value in  mainact_code_up2 and day_wkd
```

```
## Participation rates for each 10-min time slot of the day (N=43) 
##  for every value in  mainact_code_up2
```

```
## Participation rates for each 10-min time slot of the day (N=43) 
##  for every value in  mainact_code_up2
```

<img src="figures/210-rythms-1.png" title="plot of chunk rythms" alt="plot of chunk rythms" width="100%" />
