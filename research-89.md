---
title: "BIB2018"
author: "Ilse Laurijssen"
date: "10 December, 2020 21:46"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "10 December, 2020 21:45"
* cleaned data: "10 December, 2020 21:46"

## Response

* Total number of respondents: 82091

* Respondents who started the diary: 


* Respondents who finished the research: 47537

### Finished data states over time (cumulative)

![plot of chunk progress](figures/89-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/89-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|            | finished|  busy|
|:-----------|--------:|-----:|
|Vragenlijst |    47560| 34531|
|Noname      |        0| 47537|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```


















