---
title: "Corona study"
author: "Ilse Laurijssen"
date: "16 January, 2023 16:44"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "16 January, 2023 16:33"
* cleaned data: "16 January, 2023 16:44"

## Response

* Total number of respondents: 22850

* Respondents who started the diary: 
1284

* Respondents who finished the research: 502

### Finished data states over time (cumulative)

![plot of chunk progress](figures/217-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/217-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                           | finished| busy|
|:------------------------------------------|--------:|----:|
|State 0: Readin respondents                |    22850|    0|
|State 1: Invitation and reminders COVID-19 |    22849|    1|
|State 2: Pre-questionnaire                 |     3131|    0|
|State 3: Time diary                        |     2842|    0|
|State 4: Week after questionnaire          |      509|    0|
|State 5: End research                      |      502|    0|



## Background characteristics


```
## What is your gender?
```



|Var1   | Freq|
|:------|----:|
|Female |  918|
|Male   |  336|
|x      |    7|

```
## What is your current professional status?
```



|Var1                                      | Freq|
|:-----------------------------------------|----:|
|Working (this also includes interim work) |  855|
|On sick leave/maternity leave             |   34|
|Unpaid leave/career break                 |    4|
|Retired (early retirement included)       |  185|
|Looking for a first job                   |    2|
|Unfit for work                            |   30|
|Unemployed                                |   19|
|I take care of the household full-time    |   14|
|Student                                   |  134|





## Quality indicators for diaries



### How many days and complete days in the diary (cumulative)?
Note: complete days as defined by criteria (maximum amount of missing time, minimum number of activities)

![plot of chunk diarydays](figures/217-diarydays-1.png)

Table: Data table: Number of days in diary

|numberofdays | total| complete| incomplete|
|:------------|-----:|--------:|----------:|
|1            |  1284|      589|        695|
|2            |   647|      528|        119|
|3            |   568|      485|         83|
|4            |   520|      456|         64|
|5            |   491|      432|         59|
|6            |   465|      396|         69|
|7            |   449|      331|        118|

### Number of respondents with at least one diary day
Different numbers, depending on exact criterium:

* Total days (complete + incomplete), minimum 2: 647
* Complete days, at least 1 complete day: 589
* Total registered hours at least 24h: 638
* First day complete (based on criteria of maximum of undefined time, and minimum number of registrations): 524

### Total number of  diary days
* Number of complete days: 3217

### Time use results

```
## time use in minutes per day (on average)
```

```
## BASED on selection of COMPLETE DAYS
## Calculating base time parameters counting each resp 
##  for every value in  mainact_code_up2
```


```
## averaged base time parameters (N=589) 
##  for every value in  mainact_code_up2
```

<img src="figures/217-timeuse-1.png" title="plot of chunk timeuse" alt="plot of chunk timeuse" width="100%" />

Table: Data table: Average durations per type of activity

|                                                                 | weekday| weekendday|
|:----------------------------------------------------------------|-------:|----------:|
|0. Paid work, work seeking, at school, attending other train ... |   244.5|       46.6|
|1. Household work, administration, home and garden maintenan ... |   120.4|      164.4|
|2. Shopping and visiting service providers ...                   |    16.9|       18.9|
|3. Adult and childcare ...                                       |    25.3|       28.9|
|4. Sleeping, personal care and eating and drinking ...           |   649.6|      704.2|
|5. Social interaction, unpaid assistance, voluntary work, un ... |    20.3|       27.2|
|6. Going out, culture, recreation, sports and doing nothing ...  |    54.3|       78.3|
|7. Hobbies, arts and games ...                                   |    47.2|       59.6|
|8. Media use ...                                                 |   240.7|      292.4|
|9. Travel, waiting and private/unspecified time ...              |    15.3|       13.3|
|NA ...                                                           |     5.6|        6.3|

### Time use patterns

Note: graphs below are experimental.
Peek around 12PM needs to be checked ...


```
## Calculating participation for each 10-min time slot of the day counting each resp 
##  for every value in  mainact_code_up2, day_wkd
```

```
## 0. add time variables
```

```
## 1. aggregate at individual level
```

```
## 2. add extra zeros
```

```
## Participation rates for each 10-min time slot of the day (N=589) 
##  for every value in  mainact_code_up2 and day_wkd
```

```
## Participation rates for each 10-min time slot of the day (N=589) 
##  for every value in  mainact_code_up2
```

```
## Participation rates for each 10-min time slot of the day (N=589) 
##  for every value in  mainact_code_up2
```

<img src="figures/217-rythms-1.png" title="plot of chunk rythms" alt="plot of chunk rythms" width="100%" />
