---
title: "Gyermek (10-14 évesek) kutatása"
author: "Ilse Laurijssen"
date: "18 March, 2022 15:00"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "18 March, 2022 14:30"
* cleaned data: "18 March, 2022 14:30"

## Response

* Total number of respondents: 82

* Respondents who started the diary: 
19

* Respondents who finished the research: 10

### Finished data states over time (cumulative)

![plot of chunk progress](figures/317-progress-1.png)

### Activity last week
![plot of chunk lastdays](figures/317-lastdays-1.png)

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/317-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                              | finished| busy|
|:-----------------------------|--------:|----:|
|Háztartási kérdőív            |       38|   35|
|Személyi kérdőív              |       11|   27|
|Első napló nap                |       10|    1|
|Az első napló nap jellemzői   |       10|    0|
|Második napló nap             |       10|    0|
|A második napló nap jellemzői |       10|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries


```
## [1] "The quality indicators number of days and number of complete days are not available, so stopping here (for now)"
```










