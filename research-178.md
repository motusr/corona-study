---
title: "Femma meting 3"
author: "Ilse Laurijssen"
date: "10 December, 2020 21:03"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "10 December, 2020 21:01"
* cleaned data: "10 December, 2020 21:03"

## Response

* Total number of respondents: 59

* Respondents who started the diary: 
52

* Respondents who finished the research: 54

### Finished data states over time (cumulative)

![plot of chunk progress](figures/178-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/178-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                                   | finished| busy|
|:--------------------------------------------------|--------:|----:|
|STATE 1: AANKONDIGING METING 1                     |       57|    2|
|STATE 2: TUSSENPAGINA START VOORVRAGENLIJST        |       55|    2|
|STATE 3: VOORVRAGENLIJST                           |       55|    0|
|STATE 4: TUSSENPAGINA VOORVRAGENLIJST EN DAGBOEKJE |       55|    0|
|STATE 5: TIJDSGREGISTRATIE                         |       55|    0|
|STATE 6: TUSSENPAGINA DAGBOEKJE EN NAVRAGENLIJST   |       54|    1|
|STATE 7: NAVRAGENLIJST                             |       54|    0|
|EINDSCHERM ONDERZOEK METING 1                      |        0|   54|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries



### How many days and complete days in the diary (cumulative)?
Note: complete days as defined by criteria (maximum amount of missing time, minimum number of activities)

![plot of chunk diarydays](figures/178-diarydays-1.png)

Table: Data table: Number of days in diary

|numberofdays | total| complete| incomplete|
|:------------|-----:|--------:|----------:|
|1            |    52|       50|          2|
|2            |    51|       49|          2|
|3            |    50|       49|          1|
|4            |    50|       49|          1|
|5            |    50|       49|          1|
|6            |    50|       48|          2|
|7            |    49|       48|          1|

### Number of respondents with at least one diary day
Different numbers, depending on exact criterium:

* Total days (complete + incomplete), minimum 2: 51
* Complete days, at least 1 complete day: 50
* Total registered hours at least 24h: 51
* First day complete (based on criteria of maximum of undefined time, and minimum number of registrations): 49

### Total number of  diary days
* Number of complete days: 342

### Time use results

```
## time use in minutes per day (on average)
```

```
## STRICT selection of respondents: 7 complete diary days
## Calculating base time parameters counting each resp 
##  for every value in  mainact_code_up2 and day_wkd
```


```
## averaged base time parameters (N=48) 
##  for every value in  mainact_code_up2 and day_wkd
```

<img src="figures/178-timeuse-1.png" title="plot of chunk timeuse" alt="plot of chunk timeuse" width="100%" />

Table: Data table: Average durations per type of activity

|                                                           | weekday| weekendday|
|:----------------------------------------------------------|-------:|----------:|
|Betaald werk ...                                           |   319.8|       17.4|
|Huishoudelijk werk, klussen, winkelen en dienstbezoek ...  |   138.2|      192.8|
|Kinderzorg en hulp aan volwassenen (familie en andere) ... |    49.2|       48.0|
|Persoonlijke verzorging, eten en drinken ...               |   136.5|      172.6|
|Slapen, rusten en andere persoonlijke behoeften ...        |   483.8|      557.0|
|Opleiding ...                                              |     7.1|        6.4|
|Sociale contacten en verenigingsleven ...                  |    60.0|      159.3|
|Vrije tijd en media ...                                    |   127.7|      222.8|
|Wachten ...                                                |     1.7|        3.9|
|Onderweg ...                                               |   102.2|       50.5|
|Overige ...                                                |    11.8|        9.2|
|NA ...                                                     |     1.8|        0.0|

### Time use patterns

Note: graphs below are experimental.
Peek around 12PM needs to be checked ...


```
## Calculating participation for each 10-min time slot of the day counting each resp 
##  for every value in  mainact_code_up2, day_wkd
```

```
## 0. add time variables
```

```
## 1. aggregate at individual level
```

```
## 2. add extra zeros
```

```
## Participation rates for each 10-min time slot of the day (N=48) 
##  for every value in  mainact_code_up2 and day_wkd
```

```
## Participation rates for each 10-min time slot of the day (N=48) 
##  for every value in  mainact_code_up2
```

```
## Participation rates for each 10-min time slot of the day (N=48) 
##  for every value in  mainact_code_up2
```

<img src="figures/178-rythms-1.png" title="plot of chunk rythms" alt="plot of chunk rythms" width="100%" />
