---
title: "BRUXODUS G2 NL Inversie"
author: "Ilse Laurijssen"
date: "10 December, 2020 21:03"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "10 December, 2020 21:00"
* cleaned data: "10 December, 2020 21:01"

## Response

* Total number of respondents: 40

* Respondents who started the diary: 


* Respondents who finished the research: 26

### Finished data states over time (cumulative)

![plot of chunk progress](figures/123-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/123-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                  | finished| busy|
|:-----------------|--------:|----:|
|Uitnodigingsstate |       26|   14|
|Surveystate       |       22|    4|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```


















