---
title: "Supervisor Survey 2022"
author: "Ilse Laurijssen"
date: "15 June, 2022 14:30"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "15 June, 2022 14:30"
* cleaned data: "15 June, 2022 14:30"

## Response

* Total number of respondents: 246

* Respondents who started the diary: 


* Respondents who finished the research: 83

### Finished data states over time (cumulative)

![plot of chunk progress](figures/333-progress-1.png)

### Activity last week
![plot of chunk lastdays](figures/333-lastdays-1.png)

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/333-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                     | finished| busy|
|:--------------------|--------:|----:|
|Read in participants |      246|    0|
|INVITATION           |       94|  152|
|Supervisor survey    |       83|   11|
|thank you            |       83|    0|



## Background characteristics


```
## What is your gender?
```



|Var1   | Freq|
|:------|----:|
|Male   |   50|
|Female |   33|


















