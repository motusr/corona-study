---
title: "PhD Survey 2022"
author: "Ilse Laurijssen"
date: "15 June, 2022 14:30"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "15 June, 2022 14:30"
* cleaned data: "15 June, 2022 14:30"

## Response

* Total number of respondents: 1830

* Respondents who started the diary: 


* Respondents who finished the research: 836

### Finished data states over time (cumulative)

![plot of chunk progress](figures/332-progress-1.png)

### Activity last week

```
## Error in rbind(deparse.level, ...): numbers of columns of arguments do not match
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/332-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                     | finished| busy|
|:--------------------|--------:|----:|
|Read in participants |     1829|    1|
|Invitation           |      919|  910|
|PhD Survey 2022      |      836|   83|
|Thank you            |      836|    0|



## Background characteristics


```
## What is your gender?
```



|Var1                | Freq|
|:-------------------|----:|
|Female              |  413|
|Male                |  401|
|Something else      |    3|
|I don't want to say |   14|


















