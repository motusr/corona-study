---
title: "SMMS - testing"
author: "Ilse Laurijssen"
date: "29 September, 2021 16:30"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "29 September, 2021 16:30"
* cleaned data: "29 September, 2021 16:30"

## Response

* Total number of respondents: 1

* Respondents who started the diary: 


* Respondents who finished the research: 0

### Finished data states over time (cumulative)


```
## No data available for finished data states
```

### Activity last week


### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/277-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                      | finished| busy|
|:-------------------------------------|--------:|----:|
|Invitations                           |        1|    0|
|Informations générales                |        0|    1|
|Étape 1/3 : questionnaire individuel  |        0|    0|
|Étape 2/3 : le carnet de déplacements |        0|    0|
|Début de journée                      |        0|    0|
|Carnet de déplacements                |        0|    0|
|Des notifications ?                   |        0|    0|
|Clôture du carnet de déplacements     |        0|    0|
|Étape 3/3 : questionnaire de contrôle |        0|    0|
|Questionnaire de contrôle             |        0|    0|
|Fin de l'enquête                      |        0|    0|
|Noname                                |        0|    0|



## Background characteristics




















