---
title: "UNSD"
author: "Ilse Laurijssen"
date: "28 December, 2023 19:00"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "28 December, 2023 19:00"
* cleaned data: "28 December, 2023 19:00"

## Response

* Total number of respondents: 6

* Respondents who started the diary: 
6

* Respondents who finished the research: 6

### Finished data states over time (cumulative)


```
## No data available for finished data states
```

### Activity last week


### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/201-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|           | finished| busy|
|:----------|--------:|----:|
|invitation |        0|    0|
|Diary      |        0|    0|



## Background characteristics




















