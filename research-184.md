---
title: "PhD Survey 2019"
author: "Ilse Laurijssen"
date: "10 December, 2020 21:03"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "10 December, 2020 21:01"
* cleaned data: "10 December, 2020 21:02"

## Response

* Total number of respondents: 1586

* Respondents who started the diary: 


* Respondents who finished the research: 713

### Finished data states over time (cumulative)

![plot of chunk progress](figures/184-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/184-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                   | finished| busy|
|:----------------------------------|--------:|----:|
|State 0: read in PhD students      |     1586|    0|
|State 1: Invitations and reminderd |     1586|    0|
|State 2: PhD questionnaire         |      767|    0|
|State 3: Thank you page            |      713|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```


















