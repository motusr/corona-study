---
title: "Femma meting 1"
author: "Ilse Laurijssen"
date: "10 December, 2020 21:03"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "10 December, 2020 21:00"
* cleaned data: "10 December, 2020 21:02"

## Response

* Total number of respondents: 62

* Respondents who started the diary: 
57

* Respondents who finished the research: 53

### Finished data states over time (cumulative)

![plot of chunk progress](figures/96-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/96-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                                   | finished| busy|
|:--------------------------------------------------|--------:|----:|
|STATE 1: AANKONDIGING METING 1                     |       59|    0|
|STATE 2: TUSSENPAGINA START VOORVRAGENLIJST        |       58|    1|
|STATE 3: VOORVRAGENLIJST                           |       58|    0|
|STATE 4: TUSSENPAGINA VOORVRAGENLIJST EN DAGBOEKJE |       57|    1|
|STATE 5: TIJDSGREGISTRATIE                         |       55|    2|
|STATE 6: TUSSENPAGINA DAGBOEKJE EN NAVRAGENLIJST   |       55|    0|
|STATE 7: NAVRAGENLIJST                             |       53|    2|
|EINDSCHERM ONDERZOEK METING 1                      |        0|   53|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries



### How many days and complete days in the diary (cumulative)?
Note: complete days as defined by criteria (maximum amount of missing time, minimum number of activities)

![plot of chunk diarydays](figures/96-diarydays-1.png)

Table: Data table: Number of days in diary

|numberofdays | total| complete| incomplete|
|:------------|-----:|--------:|----------:|
|1            |    57|       55|          2|
|2            |    56|       54|          2|
|3            |    56|       54|          2|
|4            |    56|       54|          2|
|5            |    55|       54|          1|
|6            |    55|       52|          3|
|7            |    54|       48|          6|

### Number of respondents with at least one diary day
Different numbers, depending on exact criterium:

* Total days (complete + incomplete), minimum 2: 56
* Complete days, at least 1 complete day: 55
* Total registered hours at least 24h: 56
* First day complete (based on criteria of maximum of undefined time, and minimum number of registrations): 54

### Total number of  diary days
* Number of complete days: 371

### Time use results

```
## time use in minutes per day (on average)
```

```
## STRICT selection of respondents: 7 complete diary days
## Calculating base time parameters counting each resp 
##  for every value in  mainact_code_up2 and day_wkd
```


```
## averaged base time parameters (N=48) 
##  for every value in  mainact_code_up2 and day_wkd
```

<img src="figures/96-timeuse-1.png" title="plot of chunk timeuse" alt="plot of chunk timeuse" width="100%" />

Table: Data table: Average durations per type of activity

|                                                           | weekday| weekendday|
|:----------------------------------------------------------|-------:|----------:|
|Betaald werk ...                                           |   357.3|       35.4|
|Huishoudelijk werk, klussen, winkelen en dienstbezoek ...  |   100.0|      186.2|
|Kinderzorg en verzorging aan volwassenen ...               |    42.2|       46.9|
|Persoonlijke verzorging, eten en drinken ...               |   129.2|      158.4|
|Slapen, rusten en andere persoonlijke behoeften ...        |   468.8|      571.0|
|Opleiding ...                                              |     6.8|        6.7|
|Sociale contacten, verenigingsleven en onbetaalde hulp ... |    62.7|      121.5|
|Vrije tijd en media ...                                    |   131.5|      242.9|
|Wachten ...                                                |     2.8|        1.2|
|Onderweg ...                                               |   118.5|       58.0|
|Overige ...                                                |    14.7|       11.1|
|NA ...                                                     |     5.4|        0.6|

### Time use patterns

Note: graphs below are experimental.
Peek around 12PM needs to be checked ...


```
## Calculating participation for each 10-min time slot of the day counting each resp 
##  for every value in  mainact_code_up2, day_wkd
```

```
## 0. add time variables
```

```
## 1. aggregate at individual level
```

```
## 2. add extra zeros
```

```
## Participation rates for each 10-min time slot of the day (N=48) 
##  for every value in  mainact_code_up2 and day_wkd
```

```
## Participation rates for each 10-min time slot of the day (N=48) 
##  for every value in  mainact_code_up2
```

```
## Participation rates for each 10-min time slot of the day (N=48) 
##  for every value in  mainact_code_up2
```

<img src="figures/96-rythms-1.png" title="plot of chunk rythms" alt="plot of chunk rythms" width="100%" />
