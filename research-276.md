---
title: "SMMS - testing"
author: "Ilse Laurijssen"
date: "29 September, 2021 15:30"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "29 September, 2021 15:30"
* cleaned data: "29 September, 2021 15:30"

## Response

* Total number of respondents: 6

* Respondents who started the diary: 


* Respondents who finished the research: 0

### Finished data states over time (cumulative)


```
## No data available for finished data states
```

### Activity last week


### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/276-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                  | finished| busy|
|:-----------------|--------:|----:|
|Invitations       |        4|    2|
|general info      |        4|    0|
|Individual Survey |        0|    4|
|diary info        |        0|    0|
|pre-diary survey  |        0|    0|
|live diary        |        0|    0|
|post-diary survey |        0|    0|
|end diary 1 info  |        0|    0|
|diary2 info       |        0|    0|
|retro diary       |        0|    0|
|Noname            |        0|    0|



## Background characteristics




















