---
title: "Femma meting 2"
author: "Ilse Laurijssen"
date: "10 December, 2020 21:03"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "10 December, 2020 21:00"
* cleaned data: "10 December, 2020 21:02"

## Response

* Total number of respondents: 61

* Respondents who started the diary: 
57

* Respondents who finished the research: 53

### Finished data states over time (cumulative)

![plot of chunk progress](figures/143-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/143-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                                   | finished| busy|
|:--------------------------------------------------|--------:|----:|
|State 1: aankondiging meting                       |       61|    0|
|State 2: Tussenpagina start voorvragenlijst        |       57|    4|
|State 3: voorvragenlijst                           |       57|    0|
|State 4: Tussenpagina voorvragenlijst en dagboekje |       57|    0|
|State 5: tijdsregistratie                          |       57|    0|
|TUSSENPAGINA                                       |       56|    1|
|State 7: navragenlijst                             |       53|    3|
|Eindscherm onderzoek meting 1                      |        0|   53|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries



### How many days and complete days in the diary (cumulative)?
Note: complete days as defined by criteria (maximum amount of missing time, minimum number of activities)

![plot of chunk diarydays](figures/143-diarydays-1.png)

Table: Data table: Number of days in diary

|numberofdays | total| complete| incomplete|
|:------------|-----:|--------:|----------:|
|1            |    56|       54|          2|
|2            |    56|       51|          5|
|3            |    54|       50|          4|
|4            |    54|       50|          4|
|5            |    53|       50|          3|
|6            |    53|       47|          6|
|7            |    50|       46|          4|

### Number of respondents with at least one diary day
Different numbers, depending on exact criterium:

* Total days (complete + incomplete), minimum 2: 56
* Complete days, at least 1 complete day: 54
* Total registered hours at least 24h: 56
* First day complete (based on criteria of maximum of undefined time, and minimum number of registrations): 53

### Total number of  diary days
* Number of complete days: 348

### Time use results

```
## time use in minutes per day (on average)
```

```
## STRICT selection of respondents: 7 complete diary days
## Calculating base time parameters counting each resp 
##  for every value in  mainact_code_up2 and day_wkd
```


```
## averaged base time parameters (N=46) 
##  for every value in  mainact_code_up2 and day_wkd
```

<img src="figures/143-timeuse-1.png" title="plot of chunk timeuse" alt="plot of chunk timeuse" width="100%" />

Table: Data table: Average durations per type of activity

|                                                           | weekday| weekendday|
|:----------------------------------------------------------|-------:|----------:|
|Betaald werk ...                                           |   347.4|       29.1|
|Huishoudelijk werk, klussen, winkelen en dienstbezoek ...  |   113.0|      173.6|
|Kinderzorg en hulp aan volwassenen (familie en andere) ... |    48.5|       52.2|
|Persoonlijke verzorging, eten en drinken ...               |   131.6|      174.3|
|Slapen, rusten en andere persoonlijke behoeften ...        |   468.3|      532.6|
|Opleiding ...                                              |     3.2|        6.2|
|Sociale contacten en verenigingsleven ...                  |    60.3|      143.6|
|Vrije tijd en media ...                                    |   129.7|      236.6|
|Wachten ...                                                |     1.9|        1.1|
|Onderweg ...                                               |   117.6|       79.2|
|Overige ...                                                |    17.9|       11.4|
|NA ...                                                     |     0.5|        0.0|

### Time use patterns

Note: graphs below are experimental.
Peek around 12PM needs to be checked ...


```
## Calculating participation for each 10-min time slot of the day counting each resp 
##  for every value in  mainact_code_up2, day_wkd
```

```
## 0. add time variables
```

```
## 1. aggregate at individual level
```

```
## 2. add extra zeros
```

```
## Participation rates for each 10-min time slot of the day (N=46) 
##  for every value in  mainact_code_up2 and day_wkd
```

```
## Participation rates for each 10-min time slot of the day (N=46) 
##  for every value in  mainact_code_up2
```

```
## Participation rates for each 10-min time slot of the day (N=46) 
##  for every value in  mainact_code_up2
```

<img src="figures/143-rythms-1.png" title="plot of chunk rythms" alt="plot of chunk rythms" width="100%" />
