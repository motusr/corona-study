---
title: "Research 1 BTUS 2022"
author: "Ilse Laurijssen"
date: "09 January, 2023 13:30"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "09 January, 2023 13:30"
* cleaned data: "09 January, 2023 13:30"

## Response

* Total number of respondents: 30022

* Respondents who started the diary: 


* Respondents who finished the research: 1598

### Finished data states over time (cumulative)

![plot of chunk progress](figures/293-progress-1.png)

### Activity last week
![plot of chunk lastdays](figures/293-lastdays-1.png)

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/293-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                                           | finished|  busy|
|:----------------------------------------------------------|--------:|-----:|
|Lees in                                                    |    30019|     3|
|Aanmelden                                                  |     2397| 27622|
|Status: Informatie over het onderzoek                      |     2397|     0|
|Taak: Mijn profiel                                         |     2397|     0|
|Taak: Mijn huishouden                                      |     1775|     1|
|Taak: Huishoudvragenlijst                                  |     1665|     1|
|Status: Informatie over de volgende fase van het onderzoek |     1598|     0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```


















