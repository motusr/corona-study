---
title: "ZIJkant vzw"
author: "Ilse Laurijssen"
date: "28 December, 2023 19:00"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "28 December, 2023 19:00"
* cleaned data: "28 December, 2023 19:00"

## Response

* Total number of respondents: 219

* Respondents who started the diary: 


* Respondents who finished the research: 219

### Finished data states over time (cumulative)

![plot of chunk progress](figures/341-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/341-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|            | finished| busy|
|:-----------|--------:|----:|
|Vragenlijst |       90|  129|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```


















