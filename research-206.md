---
title: "HETUS SOURCE TM"
author: "Ilse Laurijssen"
date: "28 December, 2023 19:00"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "28 December, 2023 19:00"
* cleaned data: "28 December, 2023 19:00"

## Response

* Total number of respondents: 173

* Respondents who started the diary: 
74

* Respondents who finished the research: 62

### Finished data states over time (cumulative)

![plot of chunk progress](figures/206-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/206-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                       | finished| busy|
|:--------------------------------------|--------:|----:|
|READ IN PHASE                          |      173|    0|
|LOGIN                                  |      173|    0|
|Task 1: Household questionnaire        |       93|    0|
|Task 2: Individual questionnaire       |       85|    0|
|Task 3: Time diary                     |       85|    0|
|Task 4: End of day questionnaire       |       83|    0|
|Communication evaluation questionnaire |       73|    0|
|Task 5: Evaluation questionnaire       |       71|    0|
|Thank you page                         |       62|    0|



## Background characteristics


```
## What is your sex?
```



|Var1   | Freq|
|:------|----:|
|Male   |   27|
|Female |   51|
|X      |    2|

```
## How many members does your household count? Yourself included?
```



|Var1 | Freq|
|:----|----:|
|1    |   12|
|2    |   27|
|3    |   14|
|4    |   19|
|5    |    7|





## Quality indicators for diaries



### How many days and complete days in the diary (cumulative)?
Note: complete days as defined by criteria (maximum amount of missing time, minimum number of activities)

![plot of chunk diarydays](figures/206-diarydays-1.png)

Table: Data table: Number of days in diary

|numberofdays | total| complete| incomplete|
|:------------|-----:|--------:|----------:|
|1            |    74|       44|         30|

### Number of respondents with at least one diary day
Different numbers, depending on exact criterium:

* Total days (complete + incomplete), minimum 2: 
* Complete days, at least 1 complete day: 44
* Total registered hours at least 24h: 44
* First day complete (based on criteria of maximum of undefined time, and minimum number of registrations): 44

### Total number of  diary days
* Number of complete days: 44

### Time use results

```
## time use in minutes per day (on average)
```

```
## no diary split per day present, so stopping here (for now)
```




