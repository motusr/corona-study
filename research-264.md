---
title: "WP 2.4 Uhoo Usage and Perception survey"
author: "Ilse Laurijssen"
date: "22 June, 2021 11:30"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "22 June, 2021 11:30"
* cleaned data: "22 June, 2021 11:30"

## Response

* Total number of respondents: 79

* Respondents who started the diary: 


* Respondents who finished the research: 0

### Finished data states over time (cumulative)

![plot of chunk progress](figures/264-progress-1.png)

### Activity last week

```
## Error in charToDate(x): character string is not in a standard unambiguous format
```

```
## Error in charToDate(x): character string is not in a standard unambiguous format
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/264-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                   | finished| busy|
|:------------------|--------:|----:|
|Import respondents |       79|    0|
|Invitation mail    |       79|    0|
|Main Questionaire  |        1|   78|
|Noname             |        0|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```


















