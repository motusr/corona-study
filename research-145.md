---
title: "Femma meting 2 partner"
author: "Ilse Laurijssen"
date: "10 December, 2020 21:03"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "10 December, 2020 21:00"
* cleaned data: "10 December, 2020 21:02"

## Response

* Total number of respondents: 24

* Respondents who started the diary: 
18

* Respondents who finished the research: 17

### Finished data states over time (cumulative)

![plot of chunk progress](figures/145-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/145-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                                 | finished| busy|
|:------------------------------------------------|--------:|----:|
|STATE 1: AANKONDIGING METING 1                   |       20|    4|
|STATE 2: TUSSENPAGINA START VOORVRAGENLIJST      |       20|    0|
|STATE 3: VOORVRAGENLIJST                         |       20|    0|
|STATE 4: TUSSENPAGINA                            |       20|    0|
|STATE 5: TIJDSGREGISTRATIE                       |       19|    1|
|STATE 6: TUSSENPAGINA DAGBOEKJE EN NAVRAGENLIJST |       19|    0|
|STATE 7: NAVRAGENLIJST                           |       17|    2|
|EINDSCHERM ONDERZOEK METING 1                    |        0|   17|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries


```
## [1] "The quality indicators number of days and number of complete days are not available, so stopping here (for now)"
```










