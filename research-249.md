---
title: "Destatis - household questionnaire"
author: "Ilse Laurijssen"
date: "28 December, 2023 19:00"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "28 December, 2023 19:00"
* cleaned data: "28 December, 2023 19:00"

## Response

* Total number of respondents: 1

* Respondents who started the diary: 


* Respondents who finished the research: 1

### Finished data states over time (cumulative)


```
## 
## 
## |           | Task 1: Household questionnaire|
## |:----------|-------------------------------:|
## |2021-01-23 |                               1|
```

### Activity last week


### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/249-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                | finished| busy|
|:-------------------------------|--------:|----:|
|READ IN PHASE                   |        1|    0|
|LOGIN                           |        1|    0|
|Task 1: Household questionnaire |        1|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```


















