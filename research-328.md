---
title: "Femma vervolgonderzoek 2022 (Maart)"
author: "Ilse Laurijssen"
date: "13 April, 2022 09:30"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "13 April, 2022 09:30"
* cleaned data: "13 April, 2022 09:30"

## Response

* Total number of respondents: 55

* Respondents who started the diary: 


* Respondents who finished the research: 49

### Finished data states over time (cumulative)

![plot of chunk progress](figures/328-progress-1.png)

### Activity last week
![plot of chunk lastdays](figures/328-lastdays-1.png)

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/328-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                         | finished| busy|
|:------------------------|--------:|----:|
|Uitnodiging              |       51|    4|
|Vragenlijst              |       49|    2|
|Bedankt voor uw deelname |        0|   49|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```


















