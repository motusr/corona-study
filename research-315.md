---
title: "EMS-IT-END"
author: "Ilse Laurijssen"
date: "25 April, 2022 19:00"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "25 April, 2022 19:00"
* cleaned data: "25 April, 2022 19:00"

## Response

* Total number of respondents: 9

* Respondents who started the diary: 


* Respondents who finished the research: 8

### Finished data states over time (cumulative)

![plot of chunk progress](figures/315-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/315-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                         | finished| busy|
|:------------------------|--------:|----:|
|Usual place of residence |        9|    0|
|Usability survey I       |        8|    1|
|USABILITY END SURVEY     |        7|    1|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```


















