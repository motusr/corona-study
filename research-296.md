---
title: "Hungarian Time Use"
author: "Ilse Laurijssen"
date: "23 December, 2021 10:30"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "23 December, 2021 10:30"
* cleaned data: "23 December, 2021 10:30"

## Response

* Total number of respondents: 3

* Respondents who started the diary: 
1

* Respondents who finished the research: 1

### Finished data states over time (cumulative)

![plot of chunk progress](figures/296-progress-1.png)

### Activity last week
![plot of chunk lastdays](figures/296-lastdays-1.png)

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/296-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                   | finished| busy|
|:------------------|--------:|----:|
|TUS kezdés         |        1|    1|
|próbanapló         |        1|    0|
|Háztartási kérdőív |        1|    0|
|Személyi           |        0|    1|
|Napló              |        0|    0|
|Hétvégi napló      |        0|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries


```
## [1] "The quality indicators number of days and number of complete days are not available, so stopping here (for now)"
```










