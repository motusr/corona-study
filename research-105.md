---
title: "TLK18 - 1 day test"
author: "Ilse Laurijssen"
date: "10 December, 2020 21:03"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "10 December, 2020 21:00"
* cleaned data: "10 December, 2020 21:01"

## Response

* Total number of respondents: 29

* Respondents who started the diary: 
10

* Respondents who finished the research: 1

### Finished data states over time (cumulative)

![plot of chunk progress](figures/105-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/105-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                                               | finished| busy|
|:--------------------------------------------------------------|--------:|----:|
|State 0: Pauze om inschrijvingsmail te ontvangen               |       27|    0|
|State 1: Uitnodigingen en herinneringen TLK18                  |       24|    3|
|State 2: Profielvragenlijst TLK18                              |       19|    1|
|State 3: Spreiding veldwerk TLK18                              |       18|    0|
|State 4: Tussenpagina profiel & voorvragenlijst TLK18          |       14|    4|
|State 5: Voorvragenlijst TLK18                                 |       13|    1|
|State 6: Tussenpagina voorvragenlijst & tijdsregistratie TLK18 |       11|    0|
|State 7: Tijdsregistratie 7 dagen TLK18                        |       11|    0|
|State 8: Tussenpagina tijdsregistratie & navragenlijst TLK18   |        2|    0|
|State 9: Navragenlijst TLK18                                   |        2|    0|
|State 10: Eindscherm onderzoek TLK18                           |        1|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries


```
## [1] "The quality indicators number of days and number of complete days are not available, so stopping here (for now)"
```










