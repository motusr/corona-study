---
title: "Diamond Mediaonderzoek"
author: "Ilse Laurijssen"
date: "10 December, 2020 21:08"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "10 December, 2020 21:03"
* cleaned data: "10 December, 2020 21:08"

## Response

* Total number of respondents: 10023

* Respondents who started the diary: 
730

* Respondents who finished the research: 572

### Finished data states over time (cumulative)

![plot of chunk progress](figures/126-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/126-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                              | finished| busy|
|:---------------------------------------------|--------:|----:|
|State 0: Pauze om te starten                  |    10023|    0|
|State 1: Login                                |    10023|    0|
|State 2: Profielvragenlijst                   |     1147|    0|
|State 3: Voorvragenlijst                      |     1020|    0|
|State 4: Pauze veldwerk voor tijdsregistratie |      943|    0|
|State 5: Tussenpagina voor tijdsregistratie   |      943|    0|
|State 6: Tijdsregistratie 7 dagen             |      790|    0|
|State 7: Navragenlijst                        |      578|    0|
|State 8: Eindscherm onderzoek                 |      571|    1|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries



### How many days and complete days in the diary (cumulative)?
Note: complete days as defined by criteria (maximum amount of missing time, minimum number of activities)

![plot of chunk diarydays](figures/126-diarydays-1.png)

Table: Data table: Number of days in diary

|numberofdays | total| complete| incomplete|
|:------------|-----:|--------:|----------:|
|1            |   730|      568|        162|
|2            |   634|      537|         97|
|3            |   603|      527|         76|
|4            |   588|      510|         78|
|5            |   576|      502|         74|
|6            |   566|      461|        105|
|7            |   557|      394|        163|

### Number of respondents with at least one diary day
Different numbers, depending on exact criterium:

* Total days (complete + incomplete), minimum 2: 634
* Complete days, at least 1 complete day: 568
* Total registered hours at least 24h: 625
* First day complete (based on criteria of maximum of undefined time, and minimum number of registrations): 497

### Total number of  diary days
* Number of complete days: 3499

### Time use results

```
## time use in minutes per day (on average)
```

```
## STRICT selection of respondents: 7 complete diary days
## Calculating base time parameters counting each resp 
##  for every value in  mainact_code_up1 and day_wkd
```


```
## averaged base time parameters (N=394) 
##  for every value in  mainact_code_up1 and day_wkd
```

<img src="figures/126-timeuse-1.png" title="plot of chunk timeuse" alt="plot of chunk timeuse" width="100%" />

Table: Data table: Average durations per type of activity

|                                                                 | weekday| weekendday|
|:----------------------------------------------------------------|-------:|----------:|
|Betaald werk, werk zoeken, naar school, cursussen en opleidi ... |   395.7|       52.7|
|Slapen, persoonlijke verzorging, eten en drinken ...             |   580.7|      678.2|
|Media gebruiken ...                                              |   133.9|      176.3|
|Huishoudelijk werk, klussen, tuinieren ...                       |    80.8|      124.4|
|Kinderzorg en hulp aan andere huisgenoten ...                    |    40.4|       47.5|
|Winkelen en bezoek diensten ...                                  |    25.9|       34.8|
|Sociale contacten, onbetaalde hulp, verenigingsleven, vrijwi ... |    36.2|      106.0|
|Uitgaan, cultuur, sport, hobby's, niets doen ...                 |    58.8|      156.2|
|Verplaatsen, wachten, dagboek invullen en overig tijdsgebrui ... |    83.9|       60.3|
|NA ...                                                           |     3.6|        3.6|

### Time use patterns

Note: graphs below are experimental.
Peek around 12PM needs to be checked ...


```
## Calculating participation for each 10-min time slot of the day counting each resp 
##  for every value in  mainact_code_up1, day_wkd
```

```
## 0. add time variables
```

```
## 1. aggregate at individual level
```

```
## 2. add extra zeros
```

```
## Participation rates for each 10-min time slot of the day (N=394) 
##  for every value in  mainact_code_up1 and day_wkd
```

```
## Participation rates for each 10-min time slot of the day (N=394) 
##  for every value in  mainact_code_up1
```

```
## Participation rates for each 10-min time slot of the day (N=394) 
##  for every value in  mainact_code_up1
```

<img src="figures/126-rythms-1.png" title="plot of chunk rythms" alt="plot of chunk rythms" width="100%" />
