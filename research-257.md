---
title: "PhD survey 2021"
author: "Ilse Laurijssen"
date: "01 June, 2021 07:01"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "01 June, 2021 07:01"
* cleaned data: "01 June, 2021 07:01"

## Response

* Total number of respondents: 1687

* Respondents who started the diary: 


* Respondents who finished the research: 723

### Finished data states over time (cumulative)

![plot of chunk progress](figures/257-progress-1.png)

### Activity last week
![plot of chunk lastdays](figures/257-lastdays-1.png)

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/257-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                     | finished| busy|
|:--------------------|--------:|----:|
|Read in participants |     1687|    0|
|INVITATION           |      849|  838|
|PhD Survey 2021      |      723|  126|
|THANK YOU            |      722|    1|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```


















