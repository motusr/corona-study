---
title: "Femma vervolgonderzoek 2022 (oktober)"
author: "Ilse Laurijssen"
date: "17 November, 2022 09:30"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "17 November, 2022 09:30"
* cleaned data: "17 November, 2022 09:30"

## Response

* Total number of respondents: 54

* Respondents who started the diary: 


* Respondents who finished the research: 46

### Finished data states over time (cumulative)

![plot of chunk progress](figures/339-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/339-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                      | finished| busy|
|:---------------------|--------:|----:|
|Inlezen               |       54|    0|
|Uitnodiging           |       47|    7|
|Vragenlijst           |       46|    1|
|Bedankt voor deelname |        0|   46|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```


















