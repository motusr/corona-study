---
title: "UN Women employee survey"
author: "Ilse Laurijssen"
date: "18 January, 2022 16:18"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "12 January, 2022 19:00"
* cleaned data: "18 January, 2022 16:18"

## Response

* Total number of respondents: 349

* Respondents who started the diary: 
91

* Respondents who finished the research: 39

### Finished data states over time (cumulative)

![plot of chunk progress](figures/281-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/281-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                         | finished| busy|
|:------------------------|--------:|----:|
|READ IN PARTICIPANTS     |      310|    0|
|SEND INVITATION          |      136|  174|
|INDIVIDUAL QUESTIONNAIRE |      113|    2|
|WAIT TO START TIME DIARY |      113|    0|
|7 DAY TIME DIARY         |       53|   60|
|SEND THANK YOU EMAIL     |       39|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries



### How many days and complete days in the diary (cumulative)?
Note: complete days as defined by criteria (maximum amount of missing time, minimum number of activities)

![plot of chunk diarydays](figures/281-diarydays-1.png)

Table: Data table: Number of days in diary

|numberofdays | total| complete| incomplete|
|:------------|-----:|--------:|----------:|
|1            |    91|       57|         34|
|2            |    74|       54|         20|
|3            |    64|       46|         18|
|4            |    58|       41|         17|
|5            |    51|       40|         11|
|6            |    48|       33|         15|
|7            |    44|       25|         19|
|8            |    41|        0|         41|

### Number of respondents with at least one diary day
Different numbers, depending on exact criterium:

* Total days (complete + incomplete), minimum 2: 74
* Complete days, at least 1 complete day: 57
* Total registered hours at least 24h: 69
* First day complete (based on criteria of maximum of undefined time, and minimum number of registrations): 48

### Total number of  diary days
* Number of complete days: 296

### Time use results

```
## time use in minutes per day (on average)
```

```
## BASED on selection of COMPLETE DAYS
## Calculating base time parameters counting each resp 
##  for every value in  mainact_code_up2
```


```
## averaged base time parameters (N=57) 
##  for every value in  mainact_code_up2
```

<img src="figures/281-timeuse-1.png" title="plot of chunk timeuse" alt="plot of chunk timeuse" width="100%" />

Table: Data table: Average durations per type of activity

|                                                                 | weekday| weekendday|
|:----------------------------------------------------------------|-------:|----------:|
|Administrative, travel , ICT support and information managem ... |   116.2|        5.2|
|Human resources, finance and budget, and procurement ...         |    75.7|        3.5|
|Advisory support, information exchange, meetings, training a ... |   183.8|       21.7|
|Communication, advocacy, resource mobilisation  and coordina ... |    51.2|        3.6|
|Data, research, reporting, monitoring, and policy and progra ... |    80.2|        3.8|
|Travel, waiting (work and non-work related) ...                  |    49.8|       50.8|
|Eating and drinking, smoking ...                                 |    86.2|      117.2|
|Personal care, sleep ...                                         |   497.0|      611.6|
|Domestic work, childcare, adult care ...                         |   119.1|      283.7|
|Leisure, social participation, media usage ...                   |   112.8|      286.0|
|Study ...                                                        |    15.5|       16.2|
|Other time use ...                                               |    38.7|       25.3|
|NA ...                                                           |    13.6|       11.4|

### Time use patterns

Note: graphs below are experimental.
Peek around 12PM needs to be checked ...


```
## Calculating participation for each 10-min time slot of the day counting each resp 
##  for every value in  mainact_code_up2, day_wkd
```

```
## 0. add time variables
```

```
## 1. aggregate at individual level
```

```
## 2. add extra zeros
```

```
## Participation rates for each 10-min time slot of the day (N=57) 
##  for every value in  mainact_code_up2 and day_wkd
```

```
## Participation rates for each 10-min time slot of the day (N=57) 
##  for every value in  mainact_code_up2
```

```
## Participation rates for each 10-min time slot of the day (N=57) 
##  for every value in  mainact_code_up2
```

<img src="figures/281-rythms-1.png" title="plot of chunk rythms" alt="plot of chunk rythms" width="100%" />
