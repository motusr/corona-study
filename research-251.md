---
title: "Destatis - time diary"
author: "Ilse Laurijssen"
date: "28 December, 2023 19:00"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "28 December, 2023 19:00"
* cleaned data: "28 December, 2023 19:00"

## Response

* Total number of respondents: 1

* Respondents who started the diary: 


* Respondents who finished the research: 1

### Finished data states over time (cumulative)


```
## 
## 
## |           | Task 3: Time diary|
## |:----------|------------------:|
## |2021-04-30 |                  1|
```

### Activity last week


### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/251-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                   | finished| busy|
|:------------------|--------:|----:|
|READ IN PHASE      |        1|    0|
|LOGIN              |        1|    0|
|Task 3: Time diary |        1|    0|



## Background characteristics




















