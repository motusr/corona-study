---
title: "7 days SSB"
author: "Ilse Laurijssen"
date: "09 March, 2021 11:30"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "09 March, 2021 11:30"
* cleaned data: "09 March, 2021 11:30"

## Response

* Total number of respondents: 3

* Respondents who started the diary: 
2

* Respondents who finished the research: 0

### Finished data states over time (cumulative)


```
## 
## 
## |           | Time diary 1| Time diary 2|
## |:----------|------------:|------------:|
## |2020-11-11 |            1|            0|
```

### Activity last week


### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/238-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|              | finished| busy|
|:-------------|--------:|----:|
|READ IN PHASE |        1|    0|
|LOGIN         |        0|    1|
|Time diary 1  |        0|    0|
|Time diary 2  |        0|    0|
|Time diary 3  |        0|    0|
|Time diary 4  |        0|    0|
|Time diary 5  |        0|    0|
|Time diary 6  |        0|    0|
|Time diary 7  |        0|    0|



## Background characteristics




















