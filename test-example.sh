#!/bin/sh

run()
{
	echo "$ $@" >&2
	"$@"
}

DIR=`dirname "$0"`
if [ "$DIR" != "." ]; then
	OPTS="-C '$DIR'"
fi

run make $OPTS research_217.md
