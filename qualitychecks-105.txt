      resp          dstchange   starthour starthourscore registeredtime  
 Min.   :  1691   Min.   :0   Min.   :4   Min.   :1      Min.   : 171.0  
 1st Qu.:  1738   1st Qu.:0   1st Qu.:4   1st Qu.:1      1st Qu.: 324.8  
 Median : 24706   Median :0   Median :4   Median :1      Median : 460.0  
 Mean   : 45275   Mean   :0   Mean   :4   Mean   :1      Mean   : 690.0  
 3rd Qu.: 89697   3rd Qu.:0   3rd Qu.:4   3rd Qu.:1      3rd Qu.: 819.5  
 Max.   :132733   Max.   :0   Max.   :4   Max.   :1      Max.   :1733.0  
                                                                         
 registeredtimescore  missingtime   missingtimescore  numberofregistrations
 Min.   :0.01696     Min.   :8347   Min.   :0.01696   Min.   : 1.00        
 1st Qu.:0.03222     1st Qu.:9260   1st Qu.:0.03222   1st Qu.: 2.00        
 Median :0.04563     Median :9620   Median :0.04563   Median : 3.00        
 Mean   :0.06845     Mean   :9390   Mean   :0.06845   Mean   : 4.80        
 3rd Qu.:0.08130     3rd Qu.:9755   3rd Qu.:0.08130   3rd Qu.: 4.75        
 Max.   :0.17192     Max.   :9909   Max.   :0.17192   Max.   :16.00        
                                                                           
 numberofsleep  numberofeat  qualitychecks     qualityscore     numberofdays 
 Min.   : NA   Min.   : NA   Min.   :0.3333   Min.   :0.3446   Min.   :1.00  
 1st Qu.: NA   1st Qu.: NA   1st Qu.:0.3333   1st Qu.:0.3548   1st Qu.:1.00  
 Median : NA   Median : NA   Median :0.3333   Median :0.3638   Median :1.00  
 Mean   :NaN   Mean   :NaN   Mean   :0.3333   Mean   :0.3790   Mean   :1.30  
 3rd Qu.: NA   3rd Qu.: NA   3rd Qu.:0.3333   3rd Qu.:0.3875   3rd Qu.:1.75  
 Max.   : NA   Max.   : NA   Max.   :0.3333   Max.   :0.4479   Max.   :2.00  
 NA's   :10    NA's   :10                                                    
      resp        dstchange.day1 starthour.day1 starthourscore.day1
 Min.   :  1691   Min.   :0      Min.   :4      Min.   :1          
 1st Qu.:  1738   1st Qu.:0      1st Qu.:4      1st Qu.:1          
 Median : 24706   Median :0      Median :4      Median :1          
 Mean   : 45275   Mean   :0      Mean   :4      Mean   :1          
 3rd Qu.: 89697   3rd Qu.:0      3rd Qu.:4      3rd Qu.:1          
 Max.   :132733   Max.   :0      Max.   :4      Max.   :1          
                                                                   
 registeredtime.day1 registeredtimescore.day1 missingtime.day1
 Min.   : 171.0      Min.   :0.1187           Min.   :   0.0  
 1st Qu.: 324.8      1st Qu.:0.2255           1st Qu.: 805.8  
 Median : 460.0      Median :0.3194           Median : 980.0  
 Mean   : 604.7      Mean   :0.4199           Mean   : 835.3  
 3rd Qu.: 634.2      3rd Qu.:0.4405           3rd Qu.:1115.2  
 Max.   :1440.0      Max.   :1.0000           Max.   :1269.0  
                                                              
 missingtimescore.day1 numberofregistrations.day1 numberofsleep.day1
 Min.   :0.1187        Min.   : 1.00              Min.   : NA       
 1st Qu.:0.2255        1st Qu.: 2.00              1st Qu.: NA       
 Median :0.3194        Median : 3.00              Median : NA       
 Mean   :0.4199        Mean   : 4.70              Mean   :NaN       
 3rd Qu.:0.4405        3rd Qu.: 4.75              3rd Qu.: NA       
 Max.   :1.0000        Max.   :15.00              Max.   : NA       
                                                  NA's   :10        
 numberofeat.day1 qualitychecks.day1 qualityscore.day1 dstchange.day2
 Min.   : NA      Min.   :0.3333     Min.   :0.4125    Min.   :0     
 1st Qu.: NA      1st Qu.:0.3333     1st Qu.:0.4837    1st Qu.:0     
 Median : NA      Median :0.3333     Median :0.5463    Median :0     
 Mean   :NaN      Mean   :0.4000     Mean   :0.6133    Mean   :0     
 3rd Qu.: NA      3rd Qu.:0.3333     3rd Qu.:0.6270    3rd Qu.:0     
 Max.   : NA      Max.   :1.0000     Max.   :1.0000    Max.   :0     
 NA's   :10                                            NA's   :7     
 starthour.day2 starthourscore.day2 registeredtime.day2
 Min.   :4      Min.   :1           Min.   :240.0      
 1st Qu.:4      1st Qu.:1           1st Qu.:243.5      
 Median :4      Median :1           Median :247.0      
 Mean   :4      Mean   :1           Mean   :284.3      
 3rd Qu.:4      3rd Qu.:1           3rd Qu.:306.5      
 Max.   :4      Max.   :1           Max.   :366.0      
 NA's   :7      NA's   :7           NA's   :7          
 registeredtimescore.day2 missingtime.day2 missingtimescore.day2
 Min.   :0.1667           Min.   :1074     Min.   :0.1667       
 1st Qu.:0.1691           1st Qu.:1134     1st Qu.:0.1691       
 Median :0.1715           Median :1193     Median :0.1715       
 Mean   :0.1975           Mean   :1156     Mean   :0.1975       
 3rd Qu.:0.2128           3rd Qu.:1196     3rd Qu.:0.2128       
 Max.   :0.2542           Max.   :1200     Max.   :0.2542       
 NA's   :7                NA's   :7        NA's   :7            
 numberofregistrations.day2 numberofsleep.day2 numberofeat.day2
 Min.   :1.000              Min.   : NA        Min.   : NA     
 1st Qu.:1.000              1st Qu.: NA        1st Qu.: NA     
 Median :1.000              Median : NA        Median : NA     
 Mean   :1.333              Mean   :NaN        Mean   :NaN     
 3rd Qu.:1.500              3rd Qu.: NA        3rd Qu.: NA     
 Max.   :2.000              Max.   : NA        Max.   : NA     
 NA's   :7                  NA's   :10         NA's   :10      
 qualitychecks.day2 qualityscore.day2
 Min.   :0.3333     Min.   :0.4444   
 1st Qu.:0.3333     1st Qu.:0.4461   
 Median :0.3333     Median :0.4477   
 Mean   :0.3333     Mean   :0.4650   
 3rd Qu.:0.3333     3rd Qu.:0.4752   
 Max.   :0.3333     Max.   :0.5028   
 NA's   :7          NA's   :7        
