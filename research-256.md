---
title: "TUS2122: context vragen"
author: "Ilse Laurijssen"
date: "15 April, 2021 06:30"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "15 April, 2021 06:30"
* cleaned data: "15 April, 2021 06:30"

## Response

* Total number of respondents: 2

* Respondents who started the diary: 


* Respondents who finished the research: 1

### Finished data states over time (cumulative)


```
## 
## 
## |           | Noname| CONTEXT 1| CONTEXT 2|
## |:----------|------:|---------:|---------:|
## |2021-04-14 |      1|         1|         1|
```

### Activity last week


### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/256-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|          | finished| busy|
|:---------|--------:|----:|
|Noname    |        1|    1|
|CONTEXT 1 |        1|    0|
|CONTEXT 2 |        1|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```


















