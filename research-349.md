---
title: "Supervisor survey 2023"
author: "Ilse Laurijssen"
date: "05 June, 2023 09:30"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "05 June, 2023 09:30"
* cleaned data: "05 June, 2023 09:30"

## Response

* Total number of respondents: 476

* Respondents who started the diary: 


* Respondents who finished the research: 210

### Finished data states over time (cumulative)

![plot of chunk progress](figures/349-progress-1.png)

### Activity last week
![plot of chunk lastdays](figures/349-lastdays-1.png)

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/349-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                       | finished| busy|
|:----------------------|--------:|----:|
|Read in participants   |      476|    0|
|Invitation             |      233|  243|
|Supervisor survey 2023 |      210|   23|
|thank you              |      209|    1|



## Background characteristics


```
## What is your gender?
```



|Var1                | Freq|
|:-------------------|----:|
|Male                |  131|
|Female              |   71|
|I don't want to say |    6|


















