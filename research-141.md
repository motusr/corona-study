---
title: "RenovActive Été 2018 Agenda"
author: "Ilse Laurijssen"
date: "10 December, 2020 21:03"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "10 December, 2020 21:00"
* cleaned data: "10 December, 2020 21:02"

## Response

* Total number of respondents: 2

* Respondents who started the diary: 
2

* Respondents who finished the research: 2

### Finished data states over time (cumulative)


```
## 
## 
## |           | Noname|
## |:----------|------:|
## |2018-08-10 |      2|
```

### Activity last week


### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/141-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                | finished| busy|
|:---------------|--------:|----:|
|Pauze comm test |        2|    0|
|Noname          |        2|    0|



## Background characteristics




















