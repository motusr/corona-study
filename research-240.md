---
title: "TestBSHJoeri"
author: "Ilse Laurijssen"
date: "22 November, 2020 20:43"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "22 November, 2020 20:43"
* cleaned data: "22 November, 2020 20:43"

## Response

* Total number of respondents: 3

* Respondents who started the diary: 


* Respondents who finished the research: 0

### Finished data states over time (cumulative)


```
## No data available for finished data states
```

### Activity last week


### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/240-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                                 | finished| busy|
|:------------------------------------------------|--------:|----:|
|Starte opp undersøkelsen                         |        3|    0|
|Login                                            |        3|    0|
|Start Husholdningsspørsmål                       |        0|    3|
|Start Diary                                      |        0|    0|
|Start spørsmålene til aktivitetskodene i dagboka |        0|    0|



## Background characteristics




















