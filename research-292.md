---
title: "Research 2 BTUS 2022"
author: "Ilse Laurijssen"
date: "09 January, 2023 13:32"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "09 January, 2023 13:31"
* cleaned data: "09 January, 2023 13:32"

## Response

* Total number of respondents: 3124

* Respondents who started the diary: 
823

* Respondents who finished the research: 372

### Finished data states over time (cumulative)

![plot of chunk progress](figures/292-progress-1.png)

### Activity last week
![plot of chunk lastdays](figures/292-lastdays-1.png)

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/292-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                  | finished| busy|
|:---------------------------------|--------:|----:|
|Lees in                           |     3123|    1|
|Start studie                      |     3121|    2|
|Taak: Voorvragenlijst             |     1970|    0|
|Status: Synchronisatie huishouden |     1308|  517|
|Taak: Tijdsregistratie            |     1307|    1|
|Taak: Navragenlijst               |      376|    0|
|Status: Onderzoek afgerond        |      372|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries


```
## [1] "The quality indicators number of days and number of complete days are not available, so stopping here (for now)"
```










