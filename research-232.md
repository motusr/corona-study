---
title: "HETUS ESSnet"
author: "Ilse Laurijssen"
date: "28 December, 2023 19:00"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "28 December, 2023 19:00"
* cleaned data: "28 December, 2023 19:00"

## Response

* Total number of respondents: 144

* Respondents who started the diary: 
59

* Respondents who finished the research: 34

### Finished data states over time (cumulative)

![plot of chunk progress](figures/232-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/232-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                       | finished| busy|
|:--------------------------------------|--------:|----:|
|READ IN PHASE                          |      144|    0|
|LOGIN                                  |      144|    0|
|Task 1: Household questionnaire        |       82|    0|
|Task 2: Individual questionnaire       |       69|    0|
|Task 3: Time diary                     |       66|    0|
|Task 4: End of day questionnaire       |       65|    0|
|Communication evaluation questionnaire |       48|    0|
|Task 5: Evaluation questionnaire       |       46|    0|
|Thank you page                         |       34|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries


```
## [1] "The quality indicators number of days and number of complete days are not available, so stopping here (for now)"
```










