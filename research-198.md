---
title: "Femma meting 4"
author: "Ilse Laurijssen"
date: "22 February, 2021 21:06"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "22 februari, 2021 21:06"
* cleaned data: "22 februari, 2021 21:06"

## Response

* Total number of respondents: 57

* Respondents who started the diary: 
48

* Respondents who finished the research: 48

### Finished data states over time (cumulative)

![plot of chunk progress](figures/198-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/198-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                                   | finished| busy|
|:--------------------------------------------------|--------:|----:|
|STATE 1: AANKONDIGING METING 1                     |       53|    4|
|STATE 2: TUSSENPAGINA START VOORVRAGENLIJST        |       50|    3|
|STATE 3: VOORVRAGENLIJST                           |       49|    1|
|STATE 4: TUSSENPAGINA VOORVRAGENLIJST EN DAGBOEKJE |       49|    0|
|STATE 4.B BEN JE KLAAR VOOR HET DAGBOEKJE?         |       49|    0|
|STATE 5: TIJDSGREGISTRATIE                         |       49|    0|
|STATE 6: TUSSENPAGINA DAGBOEKJE EN NAVRAGENLIJST   |       49|    0|
|STATE 7: NAVRAGENLIJST                             |       48|    1|
|EINDSCHERM ONDERZOEK METING 1                      |        0|   48|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries



### How many days and complete days in the diary (cumulative)?
Note: complete days as defined by criteria (maximum amount of missing time, minimum number of activities)

![plot of chunk diarydays](figures/198-diarydays-1.png)

Table: Data table: Number of days in diary

|numberofdays | total| complete| incomplete|
|:------------|-----:|--------:|----------:|
|1            |    48|       47|          1|
|2            |    47|       46|          1|
|3            |    47|       44|          3|
|4            |    46|       43|          3|
|5            |    45|       43|          2|
|6            |    45|       43|          2|
|7            |    44|       42|          2|

### Number of respondents with at least one diary day
Different numbers, depending on exact criterium:

* Total days (complete + incomplete), minimum 2: 47
* Complete days, at least 1 complete day: 47
* Total registered hours at least 24h: 47
* First day complete (based on criteria of maximum of undefined time, and minimum number of registrations): 46

### Total number of  diary days
* Number of complete days: 308

### Time use results

```
## time use in minutes per day (on average)
```

```
## STRICT selection of respondents: 7 complete diary days
## Calculating base time parameters counting each resp 
##  for every value in  mainact_code_up2 and day_wkd
```


```
## averaged base time parameters (N=42) 
##  for every value in  mainact_code_up2 and day_wkd
```

<img src="figures/198-timeuse-1.png" title="plot of chunk timeuse" alt="plot of chunk timeuse" width="100%" />

Table: Data table: Average durations per type of activity

|                                                           | weekday| weekendday|
|:----------------------------------------------------------|-------:|----------:|
|Betaald werk ...                                           |   324.5|       13.1|
|Huishoudelijk werk, klussen, winkelen en dienstbezoek ...  |   120.9|      188.8|
|Kinderzorg en hulp aan volwassenen (familie en andere) ... |    51.0|       82.5|
|Persoonlijke verzorging, eten en drinken ...               |   122.8|      144.7|
|Slapen, rusten en andere persoonlijke behoeften ...        |   479.0|      576.4|
|Opleiding ...                                              |     4.6|        8.5|
|Sociale contacten en verenigingsleven ...                  |    69.3|      126.6|
|Vrije tijd en media ...                                    |   132.3|      231.0|
|Wachten ...                                                |     3.4|        0.8|
|Onderweg ...                                               |   117.2|       54.7|
|Overige ...                                                |    13.2|       11.6|
|NA ...                                                     |     1.9|        1.3|

### Time use patterns

Note: graphs below are experimental.
Peek around 12PM needs to be checked ...


```
## Calculating participation for each 10-min time slot of the day counting each resp 
##  for every value in  mainact_code_up2, day_wkd
```

```
## 0. add time variables
```

```
## 1. aggregate at individual level
```

```
## 2. add extra zeros
```

```
## Participation rates for each 10-min time slot of the day (N=42) 
##  for every value in  mainact_code_up2 and day_wkd
```

```
## Participation rates for each 10-min time slot of the day (N=42) 
##  for every value in  mainact_code_up2
```

```
## Participation rates for each 10-min time slot of the day (N=42) 
##  for every value in  mainact_code_up2
```

<img src="figures/198-rythms-1.png" title="plot of chunk rythms" alt="plot of chunk rythms" width="100%" />
