---
title: "Forum Vies Mobiles MOBI"
author: "Ilse Laurijssen"
date: "10 December, 2020 21:03"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "10 December, 2020 21:01"
* cleaned data: "10 December, 2020 21:02"

## Response

* Total number of respondents: 1459

* Respondents who started the diary: 
67

* Respondents who finished the research: 18

### Finished data states over time (cumulative)

![plot of chunk progress](figures/180-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/180-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                               | finished| busy|
|:------------------------------|--------:|----:|
|READ IN PHASE                  |     1459|    0|
|LOGIN                          |     1459|    0|
|Survey: Initial questionnaire  |      118|    0|
|Diary and geofencing           |      113|    1|
|SURVEY: after two weeks survey |       18|    0|
|GEOFENCING                     |       18|    0|
|RESEARCH COMPLETE              |       18|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries


```
## [1] "The quality indicators number of days and number of complete days are not available, so stopping here (for now)"
```










