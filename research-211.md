---
title: "Femma actieonderzoek Partner mrt 20"
author: "Ilse Laurijssen"
date: "10 December, 2020 21:03"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "10 December, 2020 21:01"
* cleaned data: "10 December, 2020 21:02"

## Response

* Total number of respondents: 20

* Respondents who started the diary: 
13

* Respondents who finished the research: 13

### Finished data states over time (cumulative)

![plot of chunk progress](figures/211-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/211-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                                   | finished| busy|
|:--------------------------------------------------|--------:|----:|
|State 1: aankondiging meting 1                     |       14|    6|
|STATE 2: TUSSENPAGINA START VOORVRAGENLIJST        |       14|    0|
|STATE 3: VOORVRAGENLIJST                           |       14|    0|
|STATE 4: TUSSENPAGINA VOORVRAGENLIJST EN DAGBOEKJE |       14|    0|
|STATE 5: TIJDSGREGISTRATIE                         |       14|    0|
|STATE 6: TUSSENPAGINA DAGBOEKJE EN NAVRAGENLIJST   |       14|    0|
|STATE 7: NAVRAGENLIJST                             |       13|    1|
|EINDSCHERM ONDERZOEK METING 1                      |        0|   13|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries



### How many days and complete days in the diary (cumulative)?
Note: complete days as defined by criteria (maximum amount of missing time, minimum number of activities)

![plot of chunk diarydays](figures/211-diarydays-1.png)

Table: Data table: Number of days in diary

|numberofdays | total| complete| incomplete|
|:------------|-----:|--------:|----------:|
|1            |    13|       13|          0|
|2            |    13|       13|          0|
|3            |    13|       13|          0|
|4            |    13|       13|          0|
|5            |    13|       13|          0|
|6            |    13|       13|          0|
|7            |    13|       13|          0|

### Number of respondents with at least one diary day
Different numbers, depending on exact criterium:

* Total days (complete + incomplete), minimum 2: 13
* Complete days, at least 1 complete day: 13
* Total registered hours at least 24h: 13
* First day complete (based on criteria of maximum of undefined time, and minimum number of registrations): 13

### Total number of  diary days
* Number of complete days: 91

### Time use results

```
## time use in minutes per day (on average)
```

```
## BASED on selection of COMPLETE DAYS
## Calculating base time parameters counting each resp 
##  for every value in  mainact_code_up2
```


```
## averaged base time parameters (N=13) 
##  for every value in  mainact_code_up2
```

<img src="figures/211-timeuse-1.png" title="plot of chunk timeuse" alt="plot of chunk timeuse" width="100%" />

Table: Data table: Average durations per type of activity

|                                                           | weekday| weekendday|
|:----------------------------------------------------------|-------:|----------:|
|Betaald werk ...                                           |   264.0|       62.0|
|Huishoudelijk werk, klussen, winkelen en dienstbezoek ...  |   120.6|      120.1|
|Kinderzorg en hulp aan volwassenen (familie en andere) ... |    45.4|       59.0|
|Persoonlijke verzorging, eten en drinken ...               |   135.0|      141.4|
|Slapen, rusten en andere persoonlijke behoeften ...        |   471.3|      560.4|
|Opleiding ...                                              |    16.3|        0.0|
|Sociale contacten en verenigingsleven ...                  |    55.5|       66.5|
|Vrije tijd en media ...                                    |   229.1|      383.8|
|Wachten ...                                                |     6.0|        0.0|
|Verplaatsingen ...                                         |    86.5|       32.9|
|Overige ...                                                |     9.6|       13.8|
|NA ...                                                     |     0.7|        0.0|

### Time use patterns

Note: graphs below are experimental.
Peek around 12PM needs to be checked ...


```
## Calculating participation for each 10-min time slot of the day counting each resp 
##  for every value in  mainact_code_up2, day_wkd
```

```
## 0. add time variables
```

```
## 1. aggregate at individual level
```

```
## 2. add extra zeros
```

```
## Participation rates for each 10-min time slot of the day (N=13) 
##  for every value in  mainact_code_up2 and day_wkd
```

```
## Participation rates for each 10-min time slot of the day (N=13) 
##  for every value in  mainact_code_up2
```

```
## Participation rates for each 10-min time slot of the day (N=13) 
##  for every value in  mainact_code_up2
```

<img src="figures/211-rythms-1.png" title="plot of chunk rythms" alt="plot of chunk rythms" width="100%" />
