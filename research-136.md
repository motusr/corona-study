---
title: "PhD Survey 2018"
author: "Ilse Laurijssen"
date: "10 December, 2020 21:03"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "10 December, 2020 21:01"
* cleaned data: "10 December, 2020 21:01"

## Response

* Total number of respondents: 1594

* Respondents who started the diary: 


* Respondents who finished the research: 769

### Finished data states over time (cumulative)

![plot of chunk progress](figures/136-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/136-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                   | finished| busy|
|:----------------------------------|--------:|----:|
|State 0: Read in Phd Students      |     1593|    1|
|State 1: Invitations and reminders |     1593|    0|
|State 2: Phd Questionnaire         |      844|    0|
|State 3: Thank you page            |      769|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```


















