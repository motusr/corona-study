---
title: "PhD Survey 2023"
author: "Ilse Laurijssen"
date: "05 June, 2023 09:30"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "05 June, 2023 09:30"
* cleaned data: "05 June, 2023 09:30"

## Response

* Total number of respondents: 2585

* Respondents who started the diary: 


* Respondents who finished the research: 0

### Finished data states over time (cumulative)

![plot of chunk progress](figures/348-progress-1.png)

### Activity last week
![plot of chunk lastdays](figures/348-lastdays-1.png)

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/348-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                    | finished| busy|
|:-------------------|--------:|----:|
|READ IN RESPONDENTS |     2585|    0|
|INVITATION          |     1632|  953|
|PhD Survey 2023     |      740|  892|
|THANK YOU           |        0|    0|



## Background characteristics


```
## What is your gender?
```



|Var1                | Freq|
|:-------------------|----:|
|Female              |  786|
|Male                |  652|
|Something else      |    4|
|I don't want to say |   24|


















