# Research specific settings
rs_numberofhours = sum(motus_settings$rs_numberofhours, na.rm=TRUE)
rs_starthour = motus_settings$rs_starthour[1]
rs_undef_value=-1
addtosave = c()

# Diary data transformations

# Diary manual cleaning - insert manual cleaning instructions here
# Hint: use functions diary_add_row, diary_change_row, diary_delete_row

# You can add post-processing code
# Write your code here!

# Diary quality indicators
# Several standard quality indicators will be added.
# The following quality checks will be computed: starthour, time and number of registrations, of eat, sleep, and missing time indicators.
# The same quality checks will also be computed per day.
# In addition (below), you can select some derived quality indicators (which are computed from the standard quality indicators).
# 

# callback function which allows for including additional quality indicators
my_quality_pr <- function(diary_per_resp, numberofhours, starthour, undef_value, ...)
{
  result <- qualitydiaryparams(diary_per_resp, numberofhours, starthour, undef_value);

# 
  result;
}

# we add the standard quality indicators
diary_q <- qualitydiaries(diary, makequalitydiaryparams=my_quality_pr, numberofhours=rs_numberofhours, starthour=rs_starthour, undef_value=rs_undef_value);

# we add the standard quality indicators, for each day
diary_q_perday <- qualitydiariesperday(diary, makequalitydiaryparams=my_quality_pr, numberofhours=24, starthour=rs_starthour, undef_value=rs_undef_value);

# number of days (with registrations)
diary_q = merge(diary_q, renamecolumn("numberofdays","I(registeredtime > 0)",
                                      aggregate(I(registeredtime>0) ~ resp, FUN=sum, data = diary_q_perday)))

# You can add additional quality indicators
# Hint: add to diary_q or diary_q_perday

# we add the quality indicators to the survey data (if present)
diary_q_perday = reshape(diary_q_perday, timevar="day", idvar="resp", direction="wide", sep=".day");
diary_qualitychecks = merge(diary_q, diary_q_perday, all=TRUE)
if (exists("survey")) {
  survey = merge(survey, diary_qualitychecks, all.x=TRUE)
} else {
  addtosave = c(addtosave, "diary_qualitychecks");
}
motus_settings$diary_qualityindicators = names(diary_q)[names(diary_q)!="resp"]
motus_settings$diary_qualityindicatorsperday = names(diary_q_perday)[names(diary_q_perday)!="resp"]

# we also compute summary statistics for the quality indicators
# for now ! output of qualitychecks; this should be integregated in dashboard
output_qualitychecks(diary_q, file="qualitychecks.txt")
output_qualitychecks(diary_q_perday, file="qualitychecks.txt")
