---
title: "TUS1 ESSnet pilot themes"
author: "Ilse Laurijssen"
date: "28 December, 2023 19:00"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "28 December, 2023 19:00"
* cleaned data: "28 December, 2023 19:00"

## Response

* Total number of respondents: 16

* Respondents who started the diary: 


* Respondents who finished the research: 6

### Finished data states over time (cumulative)

![plot of chunk progress](figures/242-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/242-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|              | finished| busy|
|:-------------|--------:|----:|
|Read in phase |       16|    0|
|Login         |       16|    0|
|Travel survey |        8|    0|
|End state     |        6|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```


















