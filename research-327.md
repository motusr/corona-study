---
title: "Statbel brede test - Research 2 BTUS 2022 7 days"
author: "Ilse Laurijssen"
date: "25 April, 2022 19:00"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "25 April, 2022 19:00"
* cleaned data: "25 April, 2022 19:00"

## Response

* Total number of respondents: 66

* Respondents who started the diary: 
31

* Respondents who finished the research: 21

### Finished data states over time (cumulative)

![plot of chunk progress](figures/327-progress-1.png)

### Activity last week
![plot of chunk lastdays](figures/327-lastdays-1.png)

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/327-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                  | finished| busy|
|:---------------------------------|--------:|----:|
|Lees in                           |       65|    0|
|Aanmelden                         |       65|    0|
|Taak: Individuele voorvragenlijst |       45|    0|
|Status: Sync groepen              |       35|    8|
|Taak: Tijdsregistratie            |       35|    0|
|Taak: Individuele navragenlijst   |       23|    1|
|Evaluatievragenlijst              |       22|    1|
|Status: Onderzoek afgerond        |       21|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries


```
## [1] "The quality indicators number of days and number of complete days are not available, so stopping here (for now)"
```










