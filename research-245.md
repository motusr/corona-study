---
title: "Prio Climat - Enthousiasme 43"
author: "Ilse Laurijssen"
date: "18 April, 2022 18:30"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "18 April, 2022 18:30"
* cleaned data: "18 April, 2022 18:30"

## Response

* Total number of respondents: 2

* Respondents who started the diary: 


* Respondents who finished the research: 1

### Finished data states over time (cumulative)

![plot of chunk progress](figures/245-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/245-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                          | finished| busy|
|:-------------------------|--------:|----:|
|Noname                    |        2|    0|
|QUESTIONNAIRE NO FEEDBACK |        2|    0|
|PHASE 1 COMPLETE          |        2|    0|
|Questionnaire Feedback    |        1|    1|
|Phase 2 complete          |        0|    1|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```


















