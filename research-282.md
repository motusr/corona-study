---
title: "Trip to CBS"
author: "Ilse Laurijssen"
date: "15 November, 2021 13:00"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "15 November, 2021 13:00"
* cleaned data: "15 November, 2021 13:00"

## Response

* Total number of respondents: 4

* Respondents who started the diary: 
3

* Respondents who finished the research: 0

### Finished data states over time (cumulative)

![plot of chunk progress](figures/282-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/282-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                           | finished| busy|
|:--------------------------|--------:|----:|
|Login                      |        1|    0|
|Test vragenlijst           |        0|    1|
|Time diary and geolocation |        0|    0|
|Noname                     |        0|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries


```
## [1] "The quality indicators number of days and number of complete days are not available, so stopping here (for now)"
```










