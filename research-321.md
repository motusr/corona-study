---
title: "TESZT"
author: "Ilse Laurijssen"
date: "11 May, 2022 12:01"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "11 May, 2022 12:00"
* cleaned data: "11 May, 2022 12:00"

## Response

* Total number of respondents: 7

* Respondents who started the diary: 
5

* Respondents who finished the research: 2

### Finished data states over time (cumulative)

![plot of chunk progress](figures/321-progress-1.png)

### Activity last week
![plot of chunk lastdays](figures/321-lastdays-1.png)

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/321-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                  | finished| busy|
|:-----------------|--------:|----:|
|Első napló        |        2|    2|
|Második napló     |        2|    0|
|Napló új          |        2|    0|
|Napló után kérdés |        0|    2|



## Background characteristics




















