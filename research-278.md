---
title: "test"
author: "Ilse Laurijssen"
date: "30 September, 2021 15:30"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "30 September, 2021 15:30"
* cleaned data: "30 September, 2021 15:30"

## Response

* Total number of respondents: 5

* Respondents who started the diary: 
2

* Respondents who finished the research: 0

### Finished data states over time (cumulative)

![plot of chunk progress](figures/278-progress-1.png)

### Activity last week
![plot of chunk lastdays](figures/278-lastdays-1.png)

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/278-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                      | finished| busy|
|:-------------------------------------|--------:|----:|
|Invitations                           |        4|    0|
|informations générales                |        3|    1|
|Étape 1/3 : questionnaire individuel  |        3|    0|
|Étape 2/3 : le carnet de déplacements |        1|    2|
|Début de journée                      |        1|    0|
|Carnet de déplacements                |        1|    0|
|Des notifications ?                   |        1|    0|
|Clôture du carnet de déplacements     |        1|    0|
|Étape 3/3 : questionnaire de contrôle |        1|    0|
|Début du questionnaire de contrôle    |        1|    0|
|Questionnaire de contrôle             |        1|    0|
|Fin de l'enquête                      |        0|    1|
|Noname                                |        0|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries


```
## [1] "The quality indicators number of days and number of complete days are not available, so stopping here (for now)"
```










