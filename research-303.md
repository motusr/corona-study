---
title: "Supervisor Survey 2022"
author: "Ilse Laurijssen"
date: "22 March, 2022 12:01"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "22 March, 2022 12:00"
* cleaned data: "22 March, 2022 12:00"

## Response

* Total number of respondents: 17

* Respondents who started the diary: 


* Respondents who finished the research: 14

### Finished data states over time (cumulative)

![plot of chunk progress](figures/303-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/303-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                  | finished| busy|
|:-----------------|--------:|----:|
|Noname            |       14|    3|
|Supervisor survey |        8|    6|



## Background characteristics


```
## What is your gender?
```



|Var1   | Freq|
|:------|----:|
|Male   |    4|
|Female |    3|
|Other  |    1|


















