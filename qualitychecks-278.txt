      resp          dstchange   starthour     starthourscore registeredtime
 Min.   :165426   Min.   :0   Min.   :4.000   Min.   :0.00   Min.   : 360  
 1st Qu.:165600   1st Qu.:0   1st Qu.:4.792   1st Qu.:0.25   1st Qu.: 615  
 Median :165773   Median :0   Median :5.583   Median :0.50   Median : 870  
 Mean   :165773   Mean   :0   Mean   :5.583   Mean   :0.50   Mean   : 870  
 3rd Qu.:165946   3rd Qu.:0   3rd Qu.:6.375   3rd Qu.:0.75   3rd Qu.:1125  
 Max.   :166120   Max.   :0   Max.   :7.167   Max.   :1.00   Max.   :1380  
                                                                           
 registeredtimescore  missingtime   missingtimescore numberofregistrations
 Min.   :0.1250      Min.   :1910   Min.   :0.1250   Min.   :3.00         
 1st Qu.:0.2135      1st Qu.:2062   1st Qu.:0.1780   1st Qu.:3.25         
 Median :0.3021      Median :2215   Median :0.2309   Median :3.50         
 Mean   :0.3021      Mean   :2215   Mean   :0.2309   Mean   :3.50         
 3rd Qu.:0.3906      3rd Qu.:2368   3rd Qu.:0.2839   3rd Qu.:3.75         
 Max.   :0.4792      Max.   :2520   Max.   :0.3368   Max.   :4.00         
                                                                          
 numberofsleep  numberofeat  qualitychecks      qualityscore     numberofdays
 Min.   : NA   Min.   : NA   Min.   :0.00000   Min.   :0.2720   Min.   :1.0  
 1st Qu.: NA   1st Qu.: NA   1st Qu.:0.08333   1st Qu.:0.3082   1st Qu.:1.5  
 Median : NA   Median : NA   Median :0.16667   Median :0.3443   Median :2.0  
 Mean   :NaN   Mean   :NaN   Mean   :0.16667   Mean   :0.3443   Mean   :2.0  
 3rd Qu.: NA   3rd Qu.: NA   3rd Qu.:0.25000   3rd Qu.:0.3805   3rd Qu.:2.5  
 Max.   : NA   Max.   : NA   Max.   :0.33333   Max.   :0.4167   Max.   :3.0  
 NA's   :2     NA's   :2                                                     
      resp        dstchange.day1 starthour.day1  starthourscore.day1
 Min.   :165426   Min.   :0      Min.   :4.000   Min.   :0.00       
 1st Qu.:165600   1st Qu.:0      1st Qu.:4.792   1st Qu.:0.25       
 Median :165773   Median :0      Median :5.583   Median :0.50       
 Mean   :165773   Mean   :0      Mean   :5.583   Mean   :0.50       
 3rd Qu.:165946   3rd Qu.:0      3rd Qu.:6.375   3rd Qu.:0.75       
 Max.   :166120   Max.   :0      Max.   :7.167   Max.   :1.00       
                                                                    
 registeredtime.day1 registeredtimescore.day1 missingtime.day1
 Min.   :360.0       Min.   :0.2500           Min.   : 470.0  
 1st Qu.:512.5       1st Qu.:0.3559           1st Qu.: 622.5  
 Median :665.0       Median :0.4618           Median : 775.0  
 Mean   :665.0       Mean   :0.4618           Mean   : 775.0  
 3rd Qu.:817.5       3rd Qu.:0.5677           3rd Qu.: 927.5  
 Max.   :970.0       Max.   :0.6736           Max.   :1080.0  
                                                              
 missingtimescore.day1 numberofregistrations.day1 numberofsleep.day1
 Min.   :0.2500        Min.   :1.0                Min.   : NA       
 1st Qu.:0.3559        1st Qu.:1.5                1st Qu.: NA       
 Median :0.4618        Median :2.0                Median : NA       
 Mean   :0.4618        Mean   :2.0                Mean   :NaN       
 3rd Qu.:0.5677        3rd Qu.:2.5                3rd Qu.: NA       
 Max.   :0.6736        Max.   :3.0                Max.   : NA       
                                                  NA's   :2         
 numberofeat.day1 qualitychecks.day1 qualityscore.day1 dstchange.day5
 Min.   : NA      Min.   :0.00000    Min.   :0.4491    Min.   :0     
 1st Qu.: NA      1st Qu.:0.08333    1st Qu.:0.4618    1st Qu.:0     
 Median : NA      Median :0.16667    Median :0.4745    Median :0     
 Mean   :NaN      Mean   :0.16667    Mean   :0.4745    Mean   :0     
 3rd Qu.: NA      3rd Qu.:0.25000    3rd Qu.:0.4873    3rd Qu.:0     
 Max.   : NA      Max.   :0.33333    Max.   :0.5000    Max.   :0     
 NA's   :2                                             NA's   :1     
 starthour.day5 starthourscore.day5 registeredtime.day5
 Min.   :4      Min.   :1           Min.   :190        
 1st Qu.:4      1st Qu.:1           1st Qu.:190        
 Median :4      Median :1           Median :190        
 Mean   :4      Mean   :1           Mean   :190        
 3rd Qu.:4      3rd Qu.:1           3rd Qu.:190        
 Max.   :4      Max.   :1           Max.   :190        
 NA's   :1      NA's   :1           NA's   :1          
 registeredtimescore.day5 missingtime.day5 missingtimescore.day5
 Min.   :0.1319           Min.   :1250     Min.   :0.1319       
 1st Qu.:0.1319           1st Qu.:1250     1st Qu.:0.1319       
 Median :0.1319           Median :1250     Median :0.1319       
 Mean   :0.1319           Mean   :1250     Mean   :0.1319       
 3rd Qu.:0.1319           3rd Qu.:1250     3rd Qu.:0.1319       
 Max.   :0.1319           Max.   :1250     Max.   :0.1319       
 NA's   :1                NA's   :1        NA's   :1            
 numberofregistrations.day5 numberofsleep.day5 numberofeat.day5
 Min.   :1                  Min.   : NA        Min.   : NA     
 1st Qu.:1                  1st Qu.: NA        1st Qu.: NA     
 Median :1                  Median : NA        Median : NA     
 Mean   :1                  Mean   :NaN        Mean   :NaN     
 3rd Qu.:1                  3rd Qu.: NA        3rd Qu.: NA     
 Max.   :1                  Max.   : NA        Max.   : NA     
 NA's   :1                  NA's   :2          NA's   :2       
 qualitychecks.day5 qualityscore.day5 dstchange.day6 starthour.day6
 Min.   :0.3333     Min.   :0.4213    Min.   :0      Min.   :4     
 1st Qu.:0.3333     1st Qu.:0.4213    1st Qu.:0      1st Qu.:4     
 Median :0.3333     Median :0.4213    Median :0      Median :4     
 Mean   :0.3333     Mean   :0.4213    Mean   :0      Mean   :4     
 3rd Qu.:0.3333     3rd Qu.:0.4213    3rd Qu.:0      3rd Qu.:4     
 Max.   :0.3333     Max.   :0.4213    Max.   :0      Max.   :4     
 NA's   :1          NA's   :1         NA's   :1      NA's   :1     
 starthourscore.day6 registeredtime.day6 registeredtimescore.day6
 Min.   :1           Min.   :220         Min.   :0.1528          
 1st Qu.:1           1st Qu.:220         1st Qu.:0.1528          
 Median :1           Median :220         Median :0.1528          
 Mean   :1           Mean   :220         Mean   :0.1528          
 3rd Qu.:1           3rd Qu.:220         3rd Qu.:0.1528          
 Max.   :1           Max.   :220         Max.   :0.1528          
 NA's   :1           NA's   :1           NA's   :1               
 missingtime.day6 missingtimescore.day6 numberofregistrations.day6
 Min.   :1220     Min.   :0.1528        Min.   :2                 
 1st Qu.:1220     1st Qu.:0.1528        1st Qu.:2                 
 Median :1220     Median :0.1528        Median :2                 
 Mean   :1220     Mean   :0.1528        Mean   :2                 
 3rd Qu.:1220     3rd Qu.:0.1528        3rd Qu.:2                 
 Max.   :1220     Max.   :0.1528        Max.   :2                 
 NA's   :1        NA's   :1             NA's   :1                 
 numberofsleep.day6 numberofeat.day6 qualitychecks.day6 qualityscore.day6
 Min.   : NA        Min.   : NA      Min.   :0.3333     Min.   :0.4352   
 1st Qu.: NA        1st Qu.: NA      1st Qu.:0.3333     1st Qu.:0.4352   
 Median : NA        Median : NA      Median :0.3333     Median :0.4352   
 Mean   :NaN        Mean   :NaN      Mean   :0.3333     Mean   :0.4352   
 3rd Qu.: NA        3rd Qu.: NA      3rd Qu.:0.3333     3rd Qu.:0.4352   
 Max.   : NA        Max.   : NA      Max.   :0.3333     Max.   :0.4352   
 NA's   :2          NA's   :2        NA's   :1          NA's   :1        
