---
title: "test usability"
author: "Ilse Laurijssen"
date: "18 October, 2021 09:30"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "18 October, 2021 09:30"
* cleaned data: "18 October, 2021 09:30"

## Response

* Total number of respondents: 1

* Respondents who started the diary: 


* Respondents who finished the research: 0

### Finished data states over time (cumulative)


```
## No data available for finished data states
```

### Activity last week


### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/283-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|               | finished| busy|
|:--------------|--------:|----:|
|Start research |        1|    0|
|survey         |        0|    1|
|Noname         |        0|    0|



## Background characteristics




















