---
title: "DIGITUS - Vief"
author: "Ilse Laurijssen"
date: "14 June, 2023 10:00"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "14 June, 2023 10:00"
* cleaned data: "14 June, 2023 10:00"

## Response

* Total number of respondents: 116

* Respondents who started the diary: 
5

* Respondents who finished the research: 17

### Finished data states over time (cumulative)

![plot of chunk progress](figures/350-progress-1.png)

### Activity last week
![plot of chunk lastdays](figures/350-lastdays-1.png)

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/350-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                      | finished| busy|
|:---------------------|--------:|----:|
|Voorvragenlijst       |       43|   72|
|Pause tussen VV en D1 |       43|    0|
|Start Dagboekje       |       16|   27|
|Dagboekje             |        5|   11|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries


```
## [1] "The quality indicators number of days and number of complete days are not available, so stopping here (for now)"
```










