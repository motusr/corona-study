---
title: "Statbel brede test - Research 1 BTUS 2022 7 days"
author: "Ilse Laurijssen"
date: "25 April, 2022 19:00"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "25 April, 2022 19:00"
* cleaned data: "25 April, 2022 19:00"

## Response

* Total number of respondents: 66

* Respondents who started the diary: 


* Respondents who finished the research: 27

### Finished data states over time (cumulative)

![plot of chunk progress](figures/326-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/326-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                                           | finished| busy|
|:----------------------------------------------------------|--------:|----:|
|Lees in                                                    |       66|    0|
|Aanmelden                                                  |       32|   34|
|Status: Informatie over het onderzoek                      |       32|    0|
|Taak: Mijn profiel                                         |       32|    0|
|Taak: Mijn huishouden                                      |       28|    0|
|Taak: Huishoudvragenlijst                                  |       27|    0|
|Status: Informatie over de volgende fase van het onderzoek |       27|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```


















