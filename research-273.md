---
title: "SMMS - Statbel Motus mobility survey"
author: "Ilse Laurijssen"
date: "25 April, 2022 19:00"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "25 April, 2022 19:00"
* cleaned data: "25 April, 2022 19:00"

## Response

* Total number of respondents: 37

* Respondents who started the diary: 
23

* Respondents who finished the research: 18

### Finished data states over time (cumulative)

![plot of chunk progress](figures/273-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/273-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                      | finished| busy|
|:-------------------------------------|--------:|----:|
|Invitations                           |       26|   11|
|Informations générales                |       26|    0|
|Étape 1/3 : questionnaire individuel  |       26|    0|
|Questions sur Motus                   |       26|    0|
|Fin de l'étape 1/3                    |       26|    0|
|Pause                                 |       26|    0|
|Étape 2/3 : le carnet de déplacements |       25|    1|
|Début de journée                      |       25|    0|
|Carnet de déplacements                |       25|    0|
|Des notifications ?                   |       25|    0|
|Fin de l'étape 2/3                    |       23|    2|
|Étape 3/3 : questionnaire de contrôle |       21|    2|
|Début du questionnaire de contrôle    |       18|    3|
|Questionnaire de contrôle             |       18|    0|
|Fin de l'enquête                      |        1|   17|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries


```
## [1] "The quality indicators number of days and number of complete days are not available, so stopping here (for now)"
```










