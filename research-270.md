---
title: "Test Joeri TUS2122"
author: "Ilse Laurijssen"
date: "16 November, 2021 08:30"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "16 November, 2021 08:30"
* cleaned data: "16 November, 2021 08:30"

## Response

* Total number of respondents: 10

* Respondents who started the diary: 
5

* Respondents who finished the research: 0

### Finished data states over time (cumulative)


```
## No data available for finished data states
```

### Activity last week


### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/270-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|        | finished| busy|
|:-------|--------:|----:|
|READ IN |        5|    0|
|LOGIN   |        3|    2|
|Diary   |        0|    3|
|Noname  |        0|    0|



## Background characteristics




















