---
title: "EMS-IT"
author: "Ilse Laurijssen"
date: "25 April, 2022 19:00"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "25 April, 2022 19:00"
* cleaned data: "25 April, 2022 19:00"

## Response

* Total number of respondents: 23

* Respondents who started the diary: 
11

* Respondents who finished the research: 2

### Finished data states over time (cumulative)

![plot of chunk progress](figures/308-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/308-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                           | finished| busy|
|:------------------------------------------|--------:|----:|
|Invitations                                |       11|    9|
|General information                        |       10|    1|
|Step 1/2 Individual Questionnaire - test   |        9|    1|
|Step 2/3: the travel diary                 |        9|    0|
|Travel Diary                               |        8|    1|
|post-diary: beginning of the reference day |        2|    6|
|End of the survey                          |        2|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries


```
## [1] "The quality indicators number of days and number of complete days are not available, so stopping here (for now)"
```










