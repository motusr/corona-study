---
title: "Belgisch Tijdbestedingsonderzoek 2021-2022"
author: "Ilse Laurijssen"
date: "14 February, 2022 09:30"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "14 February, 2022 09:30"
* cleaned data: "14 February, 2022 09:30"

## Response

* Total number of respondents: 47

* Respondents who started the diary: 


* Respondents who finished the research: 0

### Finished data states over time (cumulative)

![plot of chunk progress](figures/255-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/255-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                     | finished| busy|
|:------------------------------------|--------:|----:|
|READ IN                              |       47|    0|
|LOGIN                                |       47|    0|
|TASK: INDIVIDUAL PRE-QUESTIONNAIRE   |       26|    0|
|STATE: AWAIT OTHER HOUSEHOLD MEMBERS |       25|    0|
|STATE: STARTING DAY COMMUNICATION    |       25|    0|
|TASK: TIME-DIARY REGISTRATION        |       20|    5|
|TASK: INDIVIDUAL POST-QUESTIONNAIRE  |        0|    0|
|STATE: STUDY COMPLETED               |        0|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```


















