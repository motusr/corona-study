---
title: "HETUS OACL"
author: "Ilse Laurijssen"
date: "10 December, 2020 21:03"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "10 December, 2020 21:01"
* cleaned data: "10 December, 2020 21:02"

## Response

* Total number of respondents: 3

* Respondents who started the diary: 
3

* Respondents who finished the research: 2

### Finished data states over time (cumulative)

![plot of chunk progress](figures/204-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/204-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                  | finished| busy|
|:-----------------|--------:|----:|
|Pause to start    |        2|    0|
|Communication     |        2|    0|
|Pre-questionnaire |        2|    0|
|HETUS diary       |        2|    0|
|Post-diary        |        0|    2|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries


```
## [1] "The quality indicators number of days and number of complete days are not available, so stopping here (for now)"
```










