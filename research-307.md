---
title: "Statbel Motus ActivPAL Survey"
author: "Ilse Laurijssen"
date: "25 April, 2022 19:00"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "25 April, 2022 19:00"
* cleaned data: "25 April, 2022 19:00"

## Response

* Total number of respondents: 11

* Respondents who started the diary: 


* Respondents who finished the research: 11

### Finished data states over time (cumulative)

![plot of chunk progress](figures/307-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/307-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                    | finished| busy|
|:-------------------|--------:|----:|
|Activ Pal usability |       10|    1|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```


















