---
title: "Experimental Mobility Survey"
author: "Ilse Laurijssen"
date: "25 April, 2022 19:00"
output:
  pdf_document: default
  html_document: default
  word_document: default
---





### Data

* raw SQL data: "25 April, 2022 19:00"
* cleaned data: "25 April, 2022 19:00"

## Response

* Total number of respondents: 9

* Respondents who started the diary: 
8

* Respondents who finished the research: 7

### Finished data states over time (cumulative)

![plot of chunk progress](figures/291-progress-1.png)

### Activity last week

```
## No activity during the last 14 days
```

### How many have passed (or get stuck in) each stage?
![plot of chunk stagescum](figures/291-stagescum-1.png)

Table: Data table: Progress stages with number of respondents

|                                   | finished| busy|
|:----------------------------------|--------:|----:|
|Invitations                        |        8|    1|
|General information                |        8|    0|
|Step 1/2: individual questionnaire |        8|    0|
|End of step 1/2                    |        8|    0|
|Step 2/3: the travel diary         |        8|    0|
|Travel Diary                       |        8|    0|
|Post: start of the day             |        7|    1|
|End of the survey                  |        7|    0|



## Background characteristics


```
## Tables for background characteristics can be configured in the 'config report'
```





## Quality indicators for diaries


```
## [1] "The quality indicators number of days and number of complete days are not available, so stopping here (for now)"
```










